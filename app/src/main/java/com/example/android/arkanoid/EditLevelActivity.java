package com.example.android.arkanoid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.android.arkanoid.game.editor.CustomButton;
import com.example.android.arkanoid.game.editor.FileHelper;
import com.example.android.arkanoid.preferences.PreferenceHelper;
import com.example.android.arkanoid.preferences.SoundHelper;

public class EditLevelActivity extends AppCompatActivity {

    private CustomButton[][] matrixBtns;
    private Button btnPlay, btnLastLevel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_level);

        btnPlay = findViewById(R.id.btnPlay);
        btnLastLevel = findViewById(R.id.btnLastLevel);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        matrixBtns = new CustomButton[8][7];

        LinearLayout layoutMaster = findViewById(R.id.layoutEditor);

        for (int i = 0; i < matrixBtns.length; i++) {
            LinearLayout layoutRow = new LinearLayout(this);
            LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutRow.setLayoutParams(linearParams);
            layoutRow.setOrientation(LinearLayout.HORIZONTAL);

            for (int j = 0; j < matrixBtns[i].length;j++) {
                final Button button = new Button(this);
                button.setId((i*10)+j);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
                        (LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.height = height/16;
                params.width = (width/8);
                button.setLayoutParams(params);
                button.setBackgroundResource(android.R.drawable.btn_default);
                CustomButton temp = new CustomButton(button,0);

                matrixBtns[i][j] = temp;

                layoutRow.addView(button);
            }
            layoutMaster.addView(layoutRow);
        }

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String data = "";
                for (int i = 0; i < matrixBtns.length; i++) {
                    for (int j = 0; j<matrixBtns[i].length; j++) {
                        data+= i+" " + j + " "+ matrixBtns[i][j].getResistence()+"\n";
                    }
                }
                FileHelper.writeToFile(getApplicationContext(), data);
                Intent intent = new Intent(v.getContext(), BasicGameActivity.class);
                intent.putExtra("lifes",3);
                intent.putExtra("score",0);
                intent.putExtra("level",-2);
                startActivity(intent);
            }
        });

        btnLastLevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(FileHelper.readFromFile(getApplicationContext()) != null) {
                    Intent intent = new Intent(v.getContext(), BasicGameActivity.class);
                    intent.putExtra("lifes",3);
                    intent.putExtra("score",0);
                    intent.putExtra("level",-2);
                    startActivity(intent);
                } else {
                    Toast.makeText(EditLevelActivity.this, getResources().getString(R.string.no_level_created), Toast.LENGTH_SHORT).show();
                }
            }
        });


    }
    @Override
    protected void onPause() {
        super.onPause();
        SoundHelper.pause(R.raw.suono_home);
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(!PreferenceHelper.isSoundPrefOff(this)) {
            SoundHelper.resume(R.raw.suono_home);
        }
    }
}