package com.example.android.arkanoid;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.android.arkanoid.game.Game;
import com.example.android.arkanoid.game.UpdateThread;
import com.example.android.arkanoid.preferences.PreferenceHelper;
import com.example.android.arkanoid.preferences.SoundHelper;

import java.util.List;

public class BasicGameActivity extends AppCompatActivity {
    private Game game;
    private int level,lifes,scores,status;
    private UpdateThread myThread;
    private Handler updateHandler;
    private Button btnPause, btnHome, btnRestart,btnContinue;
    private TextView score,text, scorePopup;
    private ImageView life1,life2,life3;
    private List<Integer>list;
    FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,FrameLayout.LayoutParams.WRAP_CONTENT);
    FrameLayout.LayoutParams lg = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,FrameLayout.LayoutParams.WRAP_CONTENT);




    DialogInterface.OnClickListener dialogClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_game);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        //sets the screen orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        level = extras.getInt("level");
        lifes = extras.getInt("lifes");
        scores = extras.getInt("score");

        btnPause = findViewById(R.id.pause);
        life1 = findViewById(R.id.life1);
        life2 = findViewById(R.id.life2);
        life3 = findViewById(R.id.life3);
        score = findViewById(R.id.score);
        btnHome = findViewById(R.id.btnHome);
        btnRestart = findViewById(R.id.btnRestart);
        btnContinue = findViewById(R.id.btnContinue);
        text = findViewById(R.id.txtName);
        scorePopup = findViewById(R.id.scorePopup);
        createDialog();


        // create a new game
        lp.setMargins(0, 150, 0, 0);
        lg.setMargins(0, 5000, 0, 0);
        game = new Game(getApplicationContext(), lifes, scores, PreferenceHelper.getPreferenceController(getApplicationContext()), level);
        game.setLayoutParams(lp);
        addContentView(game, lp);

        createHandler();
        myThread = new UpdateThread(updateHandler);
        myThread.start();

        lifes = 0;
        score.setText(getResources().getString(R.string.score_game) + 0);

        btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(game.getStart()){
                    game.setStart(false);
                    game.setLayoutParams(lg);
                    text.setText(getResources().getString(R.string.pause));
                    btnPause.setBackgroundResource(R.drawable.play_button);
                    game.setStatus(3);
                }else{
                    game.setLayoutParams(lp);
                    btnPause.setBackgroundResource(R.drawable.pause_button);
                    game.setStatus(-1);

                    new CountDownTimer(1000, 1000) {

                        public void onTick(long millisUntilFinished) {
                        }

                        public void onFinish() {

                            game.setStart(true);

                        }
                    }.start();
                }
            }
        });
        btnRestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                game.setLayoutParams(lp);
                btnPause.setBackgroundResource(R.drawable.pause_button);
                game.setGameOver(true);
                game.createNewLevel();
                game.setStatus(-1);
            }
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myThread.stopThread();
                Intent intent = new Intent(BasicGameActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                game.setStatus(-1);
                game.setLayoutParams(lp);
                game.setLevel(game.getLevel()+1);
                game.createNewLevel();
            }
        });


    }

    private void showPopup(){
        if (status<0 && status>3) {
            game.setLayoutParams(lp);
            return;
        }

        switch (status) {
            case 0:{
                text.setText(getResources().getString(R.string.you_lose));
                btnContinue.setAlpha(0);
                btnContinue.setEnabled(false);
                setLayoutParams();
                break;
            }
            case 1:{
                text.setText(getResources().getString(R.string.you_win));
                btnContinue.setAlpha(1);
                btnContinue.setEnabled(true);
                setLayoutParams();
                break;
            }
            case 2: {
                text.setText(getResources().getString(R.string.game_ended));
                btnContinue.setAlpha(0);
                btnContinue.setEnabled(false);
                setLayoutParams();
                break;
            }
            case 3: {
                btnContinue.setAlpha(0);
                btnContinue.setEnabled(false);
                break;
            }
            default:{
                game.setLayoutParams(lp);
                break;
            }
        }

    }

    private void setLayoutParams() {
        game.setStart(false);
        game.setLayoutParams(lg);
    }

    private void setGame(){
        if(lifes!=list.get(1))
        {
            lifes = list.get(1);
            if(lifes == 3){
                life3.setAlpha((float)1);
                life2.setAlpha((float)1);
                life1.setAlpha((float)1);
            }
            else if(lifes == 2){
                life3.setAlpha((float)0);
                life2.setAlpha((float)1);
                life1.setAlpha((float)1);
            }
            else if(lifes == 1){
                life3.setAlpha((float)0);
                life2.setAlpha((float)0);
                life1.setAlpha((float)1);
            }
        }
        if(scores!=list.get(0))
        {
            scores = list.get(0);
            score.setText(getResources().getString(R.string.score_game) + scores);
        }

        if(status!= -1)
        {
            scorePopup.setText(getResources().getString(R.string.score_game) + "\n" +  scores);
        }

        status = list.get(2);

    }

    private void createHandler() {
        updateHandler = new Handler() {
            public void handleMessage(Message msg) {
                game.invalidate();

                list =  game.update();
                setGame();
                showPopup();
                super.handleMessage(msg);
            }
        };
    }

    protected void onPause() {
        super.onPause();
        game.stopSensing();
        btnPause.performClick();
        SoundHelper.pause(R.raw.suono_play);
    }

    protected void onResume() {
        super.onResume();
        game.startAccelerometer();
        if(!PreferenceHelper.isSoundPrefOff(this)) {
            SoundHelper.setSound(this, R.raw.suono_play);
        }
    }

    @Override
    public void onBackPressed() {
        game.setStart(false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        builder.setCancelable(false);
        builder.setMessage(this.getResources().getString(R.string.are_you_sure)).setPositiveButton(this.getResources().getString(R.string.yes), dialogClickListener).setNegativeButton( this.getResources().getString(R.string.no), dialogClickListener).show();
    }


    private void createDialog() {
         dialogClickListener= new DialogInterface.OnClickListener() {
             @Override
             public void onClick(DialogInterface dialog, int which) {
                 switch (which) {
                     case DialogInterface.BUTTON_POSITIVE:
                         myThread.stopThread();
                         SoundHelper.pauseAll();
                         finish();
                         break;

                     case DialogInterface.BUTTON_NEGATIVE:
                         game.setStart(true);
                         break;
                 }
             }
         };
    }
}