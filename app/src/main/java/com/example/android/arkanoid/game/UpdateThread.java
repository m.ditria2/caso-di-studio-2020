package com.example.android.arkanoid.game;

import android.os.Handler;

import java.util.concurrent.atomic.AtomicBoolean;

public class UpdateThread extends Thread {
    Handler updatovaciHandler;
    private  AtomicBoolean running = new AtomicBoolean(false);
    private  AtomicBoolean stopped = new AtomicBoolean(true);

    public UpdateThread(Handler uh) {
        super();
        updatovaciHandler = uh;
    }

    public void run() {
        running.set(true);
        stopped.set(false);
        while (running.get()) {
            try {
                sleep(16);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            updatovaciHandler.sendEmptyMessage(0);
        }
        stopped.set(true);
    }

    public void stopThread() {
        running.set(false);
    }

    public void interrupt() {
        running.set(false);
    }

}