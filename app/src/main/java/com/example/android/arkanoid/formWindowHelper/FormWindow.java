package com.example.android.arkanoid.formWindowHelper;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.example.android.arkanoid.R;
import com.example.android.arkanoid.firebaseHelper.RegisteredUser;
import com.example.android.arkanoid.preferences.PreferenceHelper;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Handle the form window created when the user
 * have to login or signup
 * (can be opened by the buttons on the toolbar)
 *
 */
public class FormWindow {
    public static Dialog window;
    public static Button btnSubmit, btnExitWindow;
    public static EditText editTxtPassword, editTxtUsername;

    /**
     * Creates a popup window and shows it on the screen
     *
     * @param id: set which window (signup/login) must be created (and showed)
     * @param context
     */
    public static void createWindow(String id, final Context context) {
        window = null;
        window = new Dialog(context);
        window.setCancelable(false);
        switch (id) {
            case "signup": {
                window.setContentView(R.layout.sign_up_window);
                setElementsInWindow();
                setSignupButton(context);
                break;
            }
            case "login": {
                window.setContentView(R.layout.login_window);
                setElementsInWindow();
                setLoginButton(context);
                break;
            }
            default: {
                window.dismiss();
                break;
            }
        }
    }

    /**
     * initialize the variables and button's listeners
     */
    private static void setElementsInWindow() {
        btnSubmit = window.findViewById(R.id.btnSubmit);
        editTxtPassword = window.findViewById(R.id.editTxtPassword);
        editTxtUsername = window.findViewById(R.id.editTxtUsername);
        btnExitWindow = window.findViewById(R.id.btnExitWindow);

        window.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        btnExitWindow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (window.isShowing()) {
                    window.dismiss();
                    window = null;
                }
            }
        });
    }

    /**
     * Set the behaviour of the {@link #btnSubmit} as signup button
     *
     * @param context
     */
    private static void setSignupButton(final Context context) {
        FirebaseDatabase firebase = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = firebase.getReference("registeredUsers");
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isNetworkConnected(context)){
                    if(FormWindow.validateForm(context)) {

                        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                if (snapshot.hasChild(editTxtUsername.getText().toString())) {
                                    Toast.makeText(context, context.getResources().getString(R.string.user_in_uso), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(context, context.getResources().getString(R.string.registration_completed), Toast.LENGTH_SHORT).show();
                                    String sha256hex = md5(editTxtPassword.getText().toString());
                                    myRef.child(editTxtUsername.getText().toString()).setValue(
                                            new RegisteredUser(editTxtUsername.getText().toString(), sha256hex));
                                    if(window!=null)
                                        window.dismiss();
                                    createWindow("login", context);
                                    window.show();
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {
                            }
                        });
                    }
                }
                else Toast.makeText(context, context.getResources().getString(R.string.no_connection_signup), Toast.LENGTH_SHORT).show();

            }
        });

    }

    /**
     * Set the behaviour of the {@link #btnSubmit} as login button
     *
     * @param context
     */
    private static void setLoginButton(final Context context) {
        FirebaseDatabase firebase = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = firebase.getReference("registeredUsers");
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkConnected(context))
                {
                    if(FormWindow.validateForm(context)) {
                        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                if (snapshot.hasChild(editTxtUsername.getText().toString())) {
                                    String sha256hex = md5(editTxtPassword.getText().toString());
                                    if (snapshot.child(editTxtUsername.getText().toString()).child("password").getValue(String.class)
                                            .equals(sha256hex)) {
                                        Toast.makeText(context, context.getResources().getString(R.string.logged_in), Toast.LENGTH_SHORT).show();
                                        PreferenceHelper.setUserLogged(context,editTxtUsername.getText().toString());
                                        PreferenceHelper.isLogged(context);
                                        reload(context);
                                        if(window!= null)
                                            window.dismiss();
                                        window = null;
                                    } else {
                                        Toast.makeText(context, context.getResources().getString(R.string.incorrect_user_or_password), Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(context, context.getResources().getString(R.string.incorrect_user_or_password), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {
                            }
                        });
                    }
                }
                else Toast.makeText(context, context.getResources().getString(R.string.no_connection_login), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * check if the inputs of the form are valid
     *
     * @param context
     * @return true if the form is valid, else false.
     */
    private static boolean validateForm(Context context) {

        resetColors(context);

        if (editTxtUsername.getText().toString().equals("")) {
            editTxtUsername.setHintTextColor(Color.RED);
            editTxtUsername.getBackground().mutate().setColorFilter(ContextCompat.getColor(context, R.color.red), PorterDuff.Mode.SRC_ATOP);
            return false;
        } else if (editTxtPassword.getText().toString().equals("")) {
            editTxtPassword.setHintTextColor(Color.RED);
            editTxtPassword.getBackground().mutate().setColorFilter(ContextCompat.getColor(context, R.color.red), PorterDuff.Mode.SRC_ATOP);
            return false;
        } else {
            return true;
        }

    }

    /**
     * Refresh the activity calling the form window
     * @param context
     */
    public static void reload(Context context) {
        Intent intent = new Intent(context, context.getClass());
        context.startActivity(intent);
        ((Activity)context).finish();
    }

    /**
     * Reset the inputs' color that can be changed if
     * the user has tried to insert invalid inputs
     *
     * @param context
     */
    private static void resetColors(Context context){
        editTxtUsername.setHintTextColor(context.getResources().getColor(R.color.dark_gray,null));
        editTxtUsername.getBackground().mutate().setColorFilter(ContextCompat.getColor(context, R.color.dark_gray), PorterDuff.Mode.SRC_ATOP);

        editTxtPassword.setHintTextColor(context.getResources().getColor(R.color.dark_gray,null));
        editTxtPassword.getBackground().mutate().setColorFilter(ContextCompat.getColor(context, R.color.dark_gray), PorterDuff.Mode.SRC_ATOP);
    }

    private static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public static String md5(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte[] messageDigest = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i=0; i<messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

}
