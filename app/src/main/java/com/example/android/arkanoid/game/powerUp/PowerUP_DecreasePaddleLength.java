package com.example.android.arkanoid.game.powerUp;

import android.content.Context;
import android.graphics.BitmapFactory;

import com.example.android.arkanoid.R;

public class PowerUP_DecreasePaddleLength extends AbstractPowerUP {

    public PowerUP_DecreasePaddleLength(Context context, float x, float y, float length, float height) {
        super(context, x, y, length, height);
        setAction("smaller");
        setBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.power_up_less_size));
    }

    @Override
    public float action(float paddleLength, float objectLength) {
        if (paddleLength > (objectLength) - 100) {
            paddleLength = paddleLength - 100;
        }
        return paddleLength;
    }
}
