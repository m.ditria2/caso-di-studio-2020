package com.example.android.arkanoid.recyclerView;

import android.content.Context;

import com.example.android.arkanoid.R;

/**
 * This class handle the User score and position saved on Firebase Realtime Database
 *
 * Used to Share with other apps our score and position (available only if the user is logged in)
 *
 */
public class Contact implements Comparable{
    private String name;
    private int score;
    private int pos;

    public Contact(int pos, String name, int score) {
        this.pos = pos;
        this.name = name;
        this.score = score;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }


    public String toString(Context context) {


        return context.getResources().getString(R.string.contact_score) + score + " "+
                context.getResources().getString(R.string.contact_position) +" "+ pos;

    }

    @Override
    public int compareTo(Object o) {
        return ((Contact) o).getScore() - this.getScore();
    }
}
