package com.example.android.arkanoid.databaseHelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import androidx.annotation.Nullable;



/**
 * A helper class to manage database creation and version management
 * @see android.database.sqlite.SQLiteOpenHelper
 * {@link SQLiteOpenHelper }
 *
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "arkanoid";
    private static final String TABLE_NAME_LEVEL = "levels";
    private static final String COL1_LEVEL = "id";
    private static final String COL2_LEVEL = "completed";
    SQLiteDatabase db;

    /**
     * calls the constructor of {@link SQLiteOpenHelper}
     * and opens a Writable Database
     *
     * @param context
     */
    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 1);
        db = getWritableDatabase();
    }

    /**
     * Creates the table Levels which contains 2 columns:
     * @see #COL1_LEVEL id of the level (identified by the number of the level)
     * @see #COL2_LEVEL which is:
     *     - 0 if the level is not completed
     *     - 1 if it's completed
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTableLevel = "CREATE TABLE " + TABLE_NAME_LEVEL + " (" + COL1_LEVEL +
                " TEXT, " + COL2_LEVEL + " BOOL)";
        db.execSQL(createTableLevel);

        String insert = "INSERT INTO " + TABLE_NAME_LEVEL + " VALUES(1,1)";
        db.execSQL(insert);

        for (int i = 2; i < 7; i++) {
            insert = "INSERT INTO " + TABLE_NAME_LEVEL + " VALUES("+i+",0)";
            db.execSQL(insert);
        }

    }

    /**
     * @see SQLiteOpenHelper
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_LEVEL);
        onCreate(db);
    }

    /**
     * Update the {@link #COL2_LEVEL} from 0 to 1
     * called when the level is finished
     *
     * @param level
     */
    public void updateLevel(int level) {
        level+=2;
        String query = "UPDATE " + TABLE_NAME_LEVEL + " SET " + COL2_LEVEL + "=1 WHERE " + COL1_LEVEL + "=" + level;
        db.execSQL(query);
    }

    /**
     * unlocks all the locked level (Called only in test mode)
     */
    public void unlockAllLevels() {
        db = getWritableDatabase();
        String query = "UPDATE " + TABLE_NAME_LEVEL + " SET " + COL2_LEVEL + "=1";
        db.execSQL(query);
    }
}
