package com.example.android.arkanoid.recyclerView;

/**
 * This class defines Objects received from the {@link android.database.sqlite.SQLiteDatabase}
 *
 * @see com.example.android.arkanoid.databaseHelper.DatabaseHelper
 * {@link com.example.android.arkanoid.SinglePlayerActivity}
 */
public class Level {
    private String level;
    private boolean isCompleted;


    public Level(String level) {
        this.level = level;

    }


    public Level(String level, int isCompleted){
        this(level);
        this.isCompleted = isCompleted != 0;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "level='" + level + '\'' +
                '}';
    }

}
