package com.example.android.arkanoid.preferences;

import android.content.Context;
import android.media.MediaPlayer;

import java.util.HashMap;
import java.util.Map;

/**
 * This class handle multiple background sounds playing during the game
 */
public class SoundHelper {
    private static Map<Integer, Sound> sounds = new HashMap<>();

    /**
     * Avoid creating multiple instances of the same sound.
     * Resume the sound if it already exsists.
     *
     * @param context
     * @param id: the id of the {@link android.content.res.Resources} involved
     */
    public static void setSound(Context context, int id) {
        if (!sounds.containsKey(id) && !PreferenceHelper.isSoundPrefOff(context)) {
            MediaPlayer mediaPlayer = MediaPlayer.create(context, id);
            sounds.put(id, new Sound(mediaPlayer));
        } else if (sounds.containsKey(id) && !PreferenceHelper.isSoundPrefOff(context)) {
            sounds.get(id).resume();
        }
    }

    public static void pause(int id) {
        if (sounds.get(id) != null) {
            sounds.get(id).pause();
        }
    }


    public static void resume(int id) {
        if (sounds.get(id) != null) {
            sounds.get(id).resume();
        }
    }

    public static void pauseAll() {
        for (Map.Entry<Integer, Sound> entry : sounds.entrySet()) {
            System.out.println(entry.getKey() + "/" + entry.getValue());
            entry.getValue().pause();
        }
    }


}
