package com.example.android.arkanoid.game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;

import com.example.android.arkanoid.R;
import com.example.android.arkanoid.preferences.PreferenceHelper;

/**
 * The class Brick manages all the logic of the bricks drawn down in the game
 */
public class Brick extends DrawbleObject {

    private int resistence;
    private int initialResistence;

    Bitmap[] brickTexture = new Bitmap[3];


    /**
     * Initializing all variables and set Bitmap array for different {@link #initialResistence}.
     *
     * @param context
     * @param x: coordinate on screen
     * @param y: coordinate on screen
     * @param length
     * @param height
     * @param resistence: number of hit required to crash a brick
     */
    public Brick(Context context, float x, float y, float length, float height, int resistence) {
        super(context, x, y, length, height);
        this.resistence = resistence;
        this.initialResistence = resistence;
        setInitialBitmaps();
        setBrickBitmap();
    }

    /**
     * assigns bitmaps to the brick based on its {@link #initialResistence}
     */
    private void setInitialBitmaps() {
        switch (initialResistence) {
            case 1:
                for(int i = 0; i < brickTexture.length; i++) {
                brickTexture[i] = BitmapFactory.decodeResource(getResources(),R.drawable.brick_verde);
                }

                break;
            case 2:
                for(int i = 0; i < 2; i++) {
                    brickTexture[i] = BitmapFactory.decodeResource(getResources(),R.drawable.bricks_giallo);
                }
                brickTexture[2] = BitmapFactory.decodeResource(getResources(),R.drawable.bricks_giallo_rotto);
                break;
            case 3: {
                brickTexture[0] = BitmapFactory.decodeResource(getResources(),R.drawable.brick_viola);
                brickTexture[1] = BitmapFactory.decodeResource(getResources(),R.drawable.brick_viola_rotto);
                brickTexture[2] = BitmapFactory.decodeResource(getResources(),R.drawable.brick_viola_rottox2);
                break;
            }
            default:
                for(int i = 0; i < brickTexture.length; i++) {
                    brickTexture[i] = BitmapFactory.decodeResource(getResources(),R.drawable.brick_metal);
                }
                break;
        }
    }

    /**
     * Change dinamically the bitmap of the bricks once they have been hit
     */
    public void setBrickBitmap() {
        switch (resistence) {
            case 1: {
                setBitmap(brickTexture[2]);
                break;
            }
            case 2: {
                setBitmap(brickTexture[1]);
                break;
            }

            case 3:
            default: {
                setBitmap(brickTexture[0]);
                break;
            }
        }
    }

    public int getInitialResistence() {
        return initialResistence;
    }

    public boolean isBroken() {
        if (resistence == 1) {
            return true;
        } else return false;
    }

    public void reduceResistence(){
        if(!PreferenceHelper.isSoundPrefOff(getContext())) {
            MediaPlayer mediaPlayer = MediaPlayer.create(getContext(), R.raw.brick_crepa);
            mediaPlayer.start();
        }
        this.resistence--;
    }
}
