package com.example.android.arkanoid.game;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.NonNull;

import com.example.android.arkanoid.R;
import com.example.android.arkanoid.databaseHelper.DatabaseHelper;
import com.example.android.arkanoid.game.editor.FileHelper;
import com.example.android.arkanoid.game.powerUp.AbstractPowerUP;
import com.example.android.arkanoid.preferences.PreferenceHelper;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * This class manages the dynamics of the game.
 * It calls the onDraw method, update method and
 * both the SensorManager and OnTouchEvent.
 * Each one of them is called every frame of the Game.
 * The speed of execution is defined in {@link UpdateThread}
 *
 * It links all the components of the Game
 * and makes them work together.
 *
 * {@link com.example.android.arkanoid.BasicGameActivity} supports this class
 * and inistantiate it
 *
 * @see View
 * @see SensorEventListener
 * @see View.OnTouchListener
 * @see com.example.android.arkanoid.BasicGameActivity
 */
public class Game extends View implements SensorEventListener, View.OnTouchListener {
    private int screenX, screenY;
    private Bitmap background;
    private Bitmap strechSize;
    private Bitmap wall;

    private Display display;
    private Point size;
    private Paint paint;

    private Ball ball;
    private ArrayList<Ball> balls;
    private ArrayList<Brick> bricks;
    private Paddle paddle;

    private RectF r;

    private SensorManager sManager;
    private Sensor accelerometer;
    private int lives;
    private int score;
    private int level;
    private int status;
    private Context context;

    private String controller;
    private EditorLevel editorLevel;
    private ArrayList<AbstractPowerUP> powerUp;
    private Set<AbstractPowerUP> powerUpActive;


    /////////////////////////
    private float objectLength;
    private float paddleLength;
    private float paddleHeight;
    private float ballHeight;
    private float ballLength;
    private float brickLength;
    private float brickHeight;
    /////////////////////////////


    //first touch on the screen
    private boolean start;
    private boolean gameOver;
    private boolean firstShotOfBall;
    private boolean isWallActive;
    private boolean isPaddleIncreased;
    private boolean isPaddleDecreased;
    private boolean isMagnetActive;
    private boolean firstAccessToNewRecord = false;
    private boolean firstAccessDbLevel = false;
    private boolean gameFinished;
    private boolean skip_PopUpWindow = false;
    private boolean firstAccessInGyroscope = true;

    List<Integer> list = new ArrayList<Integer>();

    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * Called only once in the game (the first time
     * we enter the {@link android.app.Activity} calling it)
     *
     * @see com.example.android.arkanoid.BasicGameActivity
     *
     * @param context
     * @param lifes
     * @param score
     * @param controller
     * @param level
     */
    public Game(Context context, int lifes, int score, String controller, int level) {
        super(context);

        //Initializing variables
        paint = new Paint();
        gameFinished = false;
        status = -1;
        this.context = context;
        this.lives = lifes;
        this.score = score;
        this.controller = controller;
        this.level = level;

        editorLevel = new EditorLevel();

        powerUp = new ArrayList<>();
        bricks = new ArrayList<>();
        balls = new ArrayList<>();
        powerUpActive = new HashSet<>();


        // set Gameover and start and start false
        start = false;
        gameOver = false;

        // creates an accelerometer and a SensorManager
        sManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        breakoutView(context);


        objectLength = screenX / 4;
        /////////////////////////
        ballHeight = screenX / 22;
        ballLength = screenY / (screenY / (ballHeight));
        paddleLength = screenX / 4;
        paddleHeight = ballHeight;
        brickLength = screenX / 7;
        brickHeight = brickLength / 2;
        /////////////////////////

        //Create a Bitmap for powerUpWall
        wall = BitmapFactory.decodeResource(getResources(), R.drawable.powerup_wall);

        //Create a paddle
        paddle = new Paddle(context, (screenX / 2) - (paddleLength / 2), size.y - 300, paddleLength, paddleHeight);


        //create a ball object and insert into arraylist lopticka
        ball = new Ball(context, (screenX / 2) - (ballLength / 2), paddle.getY() - ballHeight - 1,
                ballHeight, ballLength);

        balls.add(ball);

        this.setOnTouchListener(this);
        createNewLevel();
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * fills up the list with bricks
     *
     * the value {@link #level} = (-2) is passed only by
     * {@link com.example.android.arkanoid.EditLevelActivity}
     * to make the game finish once the level is over
     *
     * @param context
     */
    private void createBricks(Context context) {

        int[][] tempBricks;

        if (level == -2) {
            //Edited level
            int[][] temp = FileHelper.readFromFile(context);
            tempBricks = new int[8][7];

            for (int i = 0; i < tempBricks.length; i++) {
                for (int j = 0; j < tempBricks[i].length; j++) {
                    tempBricks[i][j] = temp[i][j];
                }
            }

        } else {
            //Basic level of single player
            tempBricks = editorLevel.createLevel(level);
        }

        //fill list bricks if there is a level to play (else game finished)
        if (tempBricks != null) {
            for (int i = 0; i < tempBricks.length; i++) {
                for (int j = 0; j < tempBricks[i].length; j++) {
                    if (tempBricks[i][j] != 0) {
                        bricks.add(new Brick(context, j * (brickLength + 1), i * (brickHeight + 1),
                                brickLength, brickHeight, tempBricks[i][j]));
                    }
                }
            }
        } else {
            gameFinished = true;
        }

    }

    /**
     * Sets the background and screen display variables
     * @param context
     */
    private void breakoutView(Context context) {
        background = Bitmap.createBitmap(BitmapFactory.decodeResource(this.getResources(), R.drawable.sfondo_game55));
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = wm.getDefaultDisplay();
        size = new Point();
        display.getSize(size);

        //set the point (X,Y) to the size of the screen
        screenX = size.x;
        screenY = size.y;
    }

    /**
     * Called each frame, it controls if a ball is touching an edge
     * of the screen. For the bottom edge the player loses a life
     */
    private void checkEdges() {
        for (int i = 0; i < balls.size(); i++) {
            Ball temp_ball = balls.get(i);
            if ((temp_ball.getY() + temp_ball.getySpeed() >= size.y - ballHeight) || temp_ball.getY() < -500) {
                balls.remove(temp_ball);
                //lose a life
                if (balls.size() == 0) {
                    checkLives();
                }
            }
            else if (temp_ball.getX() + temp_ball.getxSpeed() >= size.x - ballLength) {
                temp_ball.changeDirection("right"); //destra
            } else if (temp_ball.getX() + temp_ball.getxSpeed() <= 0) {
                temp_ball.changeDirection("left"); //sinistra
            } else if (temp_ball.getY() + temp_ball.getySpeed() < 10) {
                temp_ball.changeDirection("up"); //sopra
            }
            if (isWallActive) {
                if (temp_ball.getY() + temp_ball.getySpeed() > paddle.getY() + 30) {
                    temp_ball.oppositeYSpeed();
                }
            }
        }
    }

    /**
     * checks the status of the game: if my lives or the game are over
     */
    private void checkLives() {
        if (lives == 1) {
            //no lives
            gameOver = true;
            start = false;
            invalidate();
        } else {
            //game over
            lives--;
            start = false;
            setVariablesOnStart();
        }
    }


    /**
     * called each frame of the game: makes all the logic.
     * It makes each component having speed to move
     * and checks all the collision
     *
     * @return a list with current lives, score, status of the game
     */
    public List<Integer> update() {
        if (start) {
            if (bricksIsEmpty()) start = false;

            checkEdges();
            powerUpMoves();

            checkPowerUpActive();
            //for each ball on the screen ( in arraylist balls)
            for (Ball temp_ball : balls) {
                //logic of the ball
                if (isMagnetActive && !temp_ball.getBallOnMagnet()) {//se il PowerUp "Magnete" è attivo
                    temp_ball.isNearToPaddleMagnet(paddle.getX(), paddle.getY(), paddleLength, paddleHeight); //usa il paddle con il magnete
                } else if (!isMagnetActive && temp_ball.getBallOnMagnet()) {
                    temp_ball.setBallOnMagnet(false);
                    temp_ball.setY(paddle.getY() - ballHeight - 30);
                    temp_ball.generateSpeed();
                } else {
                    temp_ball.isNearToPaddle(paddle.getX(), paddle.getY(), paddleLength, paddleHeight);//usa il paddle normale
                }

                //ball moves
                temp_ball.shift();
            }

            //For each brick check if ball is colliding with it
            for (int i = 0; i < bricks.size(); i++) {
                Brick brick = bricks.get(i);
                for (Ball temp_ball : balls) {
                    if (temp_ball.isTouchingBrick(context, brick, bricks, powerUp)) {
                        if (brick.getInitialResistence() != -1) {
                            score += 80;
                        }
                    }
                }
            }
        }
        list.add(0, score);
        list.add(1, lives);
        list.add(2, status);
        return list;
    }

    public void stopSensing() {
        sManager.unregisterListener(this);
    }

    public void startAccelerometer() {
        sManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
    }

    /**
     * Touchpad mode
     * Used only if the Controller preference in {@link PreferenceHelper} is set to "touchpad"
     * It captures all the touching events:
     * - ACTION_DOWN: called when the player touches the screen
     * - ACTION_MOVE: it starts when the player moves the finger on the screen
     * - ACTION_UP: called when the interaction ends and the finger is lifted up from the screen
     *
     * @param event
     * @return
     *
     * @see View.OnTouchListener
     */
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_MOVE: {
                if (skip_PopUpWindow == false) {
                    setPaddle(event);
                    attachBallOnPaddle();
                }
                break;
            }
            case MotionEvent.ACTION_UP:
                if (bricksIsEmpty()) {
                    start = false;
                }
                if (skip_PopUpWindow == false) {
                    shootBallFromPaddle();
                } else skip_PopUpWindow = false;
                break;
        }
        return true;
    }

    /**
     * Set the position of the paddle (including touching the edges of the screen)
     * @param event
     */
    private void setPaddle(MotionEvent event) {
        if (controller.equals("touchpad") & !bricksIsEmpty()) {
            if (event.getX() > screenX - paddleLength / 2) {
                paddle.setX(screenX - paddleLength);
            } else if (event.getX() < paddleLength / 2) {
                paddle.setX(0);
            } else {
                paddle.setX(event.getX() - paddleLength / 2);
            }
        }
    }

    /**
     * Force the ball to stay on the paddle only if {@link #isMagnetActive} = true
     */
    private void attachBallOnPaddle() {
        if (isMagnetActive) {
            for (Ball temp_ball : balls) {
                if (temp_ball.getBallOnMagnet()) {
                    temp_ball.setX(paddle.getX() + temp_ball.getDistanceToPaddle());
                }
            }
        }
    }

    /**
     * Shoot the ball
     *
     * @see #attachBallOnPaddle()
     */
    private void shootBallFromPaddle() {
        if (isMagnetActive) {
            for (Ball temp_ball : balls) {
                if (temp_ball.getBallOnMagnet()) {
                    if (!firstShotOfBall) {
                        temp_ball.setY(paddle.getY() - ballHeight - 30);
                    }
                    temp_ball.generateSpeed();
                    temp_ball.setBallOnMagnet(false);
                }
            }
        }
        if (firstShotOfBall) {
            isMagnetActive = false;
            firstShotOfBall = false;
        }
    }

    /**
     * Change accelerometer
     * @param event
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        if ((controller.equals("gyroscope") || ((controller.equals("giroscopio")))) && !(bricksIsEmpty())) {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                attachBallOnPaddle();
                if (firstAccessInGyroscope) {
                    start = true;
                    firstAccessInGyroscope = false;
                }
                //moves the paddle to the left and the right
                paddle.setX(paddle.getX() - (event.values[0] * 3));

                //elude the paddle to go through the right side of the screen
                if (paddle.getX() + event.values[0] > size.x - paddleLength) {
                    paddle.setX(size.x - paddleLength);
                    //elude the paddle to go through the left side of the screen
                } else if (paddle.getX() - event.values[0] <= 20) {
                    paddle.setX(0);
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    /**
     * Create a new level
     */
    public void createNewLevel() {
        bricks.clear();
        createBricks(context);
        firstAccessDbLevel = false;
        setVariablesOnStart();
        firstAccessInGyroscope = true;

    }

    /**
     * Checks for each power up set which one is active and
     * removes it if it's not active anymore
     */
    public void checkPowerUpActive() {
        //Avoiding null pointer exception of array
        ArrayList<AbstractPowerUP> temp_Array = new ArrayList(powerUpActive);
        for (int i = 0; i < temp_Array.size(); i++) {
            AbstractPowerUP temp_powerUP = temp_Array.get(i);
            if (temp_powerUP.getAction() == "wall") {
                if (isWallActive & temp_powerUP != null) isWallActive = temp_powerUP.isActive();
                else powerUpActive.remove(temp_powerUP);
            }
            if (temp_powerUP.getAction() == "magnet") {
                if (isMagnetActive & temp_powerUP != null) isMagnetActive = temp_powerUP.isActive();
                else powerUpActive.remove(temp_powerUP);
            }
            if (temp_powerUP.getAction() == "bigger") {
                if (isPaddleIncreased & temp_powerUP != null)
                    isPaddleIncreased = temp_powerUP.isActive();
                else {
                    powerUpActive.remove(temp_powerUP);
                    paddleLength = objectLength;
                }
            }
            if (temp_powerUP.getAction() == "smaller") {
                if (isPaddleDecreased & temp_powerUP != null)
                    isPaddleDecreased = temp_powerUP.isActive();
                else {
                    powerUpActive.remove(temp_powerUP);
                    paddleLength = objectLength;
                }
            }
        }
    }

    /**
     * Resets timer of the Power ups
     * @param action
     */
    public void resetPowerUPTimer(String action) {
        for (AbstractPowerUP temp_powerUP : powerUpActive) {
            if (temp_powerUP.getAction() == action) temp_powerUP.resetTimer();
        }
    }

    /**
     * Removes the power up smaller or bigger
     */
    public void resetPaddlePowerUP() {
        ArrayList<AbstractPowerUP> temp_Array = new ArrayList(powerUpActive);
        for (int i = 0; i < temp_Array.size(); i++) {
            if (temp_Array.get(i).getAction() == "smaller" || (temp_Array.get(i).getAction() == "bigger"))
                powerUpActive.remove(temp_Array.get(i));
        }
    }

    /**
     * Makes the power ups move and set the behaviour of each one of them
     */
    public void powerUpMoves() {
        for (int i = 0; i < powerUp.size(); i++) {
            AbstractPowerUP currentPowerUp = powerUp.get(i);
            currentPowerUp.shift();
            if (powerUp.size() > 0)
                if (currentPowerUp.isCloseToPaddle(paddle.getX(), paddle.getY(), paddleLength)) {
                    powerUp.remove(currentPowerUp);
                    switch (currentPowerUp.getAction()) {
                        case "life": {
                            lives = currentPowerUp.action(lives);
                            break;
                        }
                        case "ball": {
                            balls = currentPowerUp.action(context, balls);
                            break;
                        }
                        case "wall": {
                            if (powerUpActive.add(currentPowerUp)) isWallActive = true;
                            else resetPowerUPTimer(currentPowerUp.getAction());
                            break;
                        }
                        case "bigger": {
                            if (objectLength > paddleLength) {
                                resetPaddlePowerUP();
                                paddleLength = objectLength;
                            } else {
                                if (powerUpActive.add(currentPowerUp)) {
                                    paddleLength = currentPowerUp.action(paddleLength, objectLength);
                                    isPaddleIncreased = true;
                                } else {
                                    if (objectLength > paddleLength) paddleLength = objectLength;
                                    else resetPowerUPTimer(currentPowerUp.getAction());
                                }
                            }
                            break;
                        }
                        case "smaller": {
                            if (objectLength < paddleLength) {
                                resetPaddlePowerUP();
                                paddleLength = objectLength;
                            } else {
                                if (powerUpActive.add(currentPowerUp)) {
                                    paddleLength = currentPowerUp.action(paddleLength, objectLength);
                                    isPaddleDecreased = true;
                                } else resetPowerUPTimer(currentPowerUp.getAction());
                            }
                            break;
                        }
                        case "magnet": {
                            if (powerUpActive.add(currentPowerUp)) isMagnetActive = true;
                            else resetPowerUPTimer(currentPowerUp.getAction());
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                } else if (currentPowerUp.getY() > screenY) {
                    powerUp.remove(currentPowerUp);
                }
        }
    }

    /**
     * Check if the list of bricks is empty (excluding indestructible bricks)
     * @return
     */
    public boolean bricksIsEmpty() {

        for (int i = 0; i < bricks.size(); i++) {

            if (bricks.get(i).getInitialResistence() > 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Called each frame of the game: draws all the objects on the screen
     * @param canvas
     */
    protected void onDraw(Canvas canvas) {

        if (strechSize == null) {
            strechSize = Bitmap.createScaledBitmap(background, size.x, size.y, false);
        }
        canvas.drawBitmap(strechSize, 0, 0, paint);

        // draw the ball
        for (int i = 0; i < balls.size(); i++) {
            Ball b = balls.get(i);
            paint.setColor(Color.RED);
            canvas.drawBitmap(b.getBitmap(), b.getX(), b.getY(), paint);

        }
        // draw the paddle
        r = new RectF(paddle.getX(), paddle.getY(), paddle.getX() + paddleLength, paddle.getY() + paddleHeight);
        canvas.drawBitmap(paddle.getBitmap(), null, r, paint);

        // draw the bricks
        for (int i = 0; i < bricks.size(); i++) {
            Brick b = bricks.get(i);
            r = new RectF(b.getX(), b.getY(), b.getX() + brickLength, b.getY() + brickHeight);
            canvas.drawBitmap(b.getBitmap(), null, r, paint);
        }

        // In case of loss
        if (gameOver) {
            newRecord();
            status = 0;
        }
        //If you won the game
        else if (bricksIsEmpty()) {
            //there are no more levels
            if (gameFinished) {
                status = 2;
                newRecord();

            } else {
                // There is next level, then unlock it
                if (!firstAccessDbLevel) {

                    firstAccessDbLevel = true;
                    unlockNextLevel();
                }
                status = 1;
            }
        }


        //Draws power Ups active
        if (powerUp != null || powerUp.size() != 0) {
            for (AbstractPowerUP item : powerUp) {
                r = new RectF(item.getX(), item.getY(), item.getX() + brickLength, item.getY() + brickHeight);
                canvas.drawBitmap(item.getBitmap(), null, r, paint);
            }
        }

        //Draw a line on the paddle if power up MAGNET is active
        if (isMagnetActive & !firstShotOfBall) {
            paint.setColor(Color.RED);
            canvas.drawLine(paddle.getX(), paddle.getY() - ballLength / 2, paddle.getX() + paddleLength, paddle.getY() - ballHeight / 2, paint);
        }

        //Draw a line under the paddle (covers all the x coordinate of the screen) if
        //the power up WALL is active
        if (isWallActive) {
            paint.setColor(Color.WHITE);
            r = new RectF(0, paddle.getY() + 50, screenX, paddle.getY() + 100);
            canvas.drawBitmap(wall, null, r, paint);
        }
    }

    /**
     * Access to SQLite Db to unlock the next level
     */
    private void unlockNextLevel() {
        DatabaseHelper db = new DatabaseHelper(context);
        db.updateLevel(level);
        db.close();
    }

    /**
     * Reset all the variables to the default values.
     */
    private void setVariablesOnStart() {
        start = true;
        firstAccessToNewRecord = false;
        isMagnetActive = true;
        firstShotOfBall = true;
        isWallActive = false;
        isPaddleIncreased = false;
        isPaddleDecreased = false;
        balls.clear();
        powerUp.clear();
        ball.setX((size.x / 2) - (ballLength / 2));
        ball.setY(paddle.getY() - ballHeight - 1);
        balls.add(ball);
        paddle.setX((screenX / 2) - (objectLength / 2));
        paddle.setY(size.y - 300);

        if (gameOver) {
            score = 0;
            lives = 3;
            gameOver = false;
        }

    }

    public void setStart(boolean start) {
        this.start = start;
    }

    public boolean getStart() {
        return start;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    /**
     * Access to Firebase Realtime Database to save the score if it's the best one
     * of the user (valid only if the user is logged in)
     */
    public void newRecord() {
        if (!firstAccessToNewRecord && PreferenceHelper.isLogged(context)) {
            firstAccessToNewRecord = true;
            FirebaseDatabase firebase = FirebaseDatabase.getInstance();
            final DatabaseReference myRef = firebase.getReference("registeredUsers").child(PreferenceHelper.getUserLogged(context));
            myRef.addListenerForSingleValueEvent(new ValueEventListener(){
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.child("score").getValue(Integer.class) != null
                            && snapshot.child("score").getValue(Integer.class) < score) {
                        myRef.child("score").setValue(score);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
    }
}
