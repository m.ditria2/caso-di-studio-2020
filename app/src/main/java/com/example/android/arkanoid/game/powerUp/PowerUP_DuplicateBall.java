package com.example.android.arkanoid.game.powerUp;

import android.content.Context;
import android.graphics.BitmapFactory;

import com.example.android.arkanoid.R;
import com.example.android.arkanoid.game.Ball;

import java.util.ArrayList;

public class PowerUP_DuplicateBall extends AbstractPowerUP {

    public PowerUP_DuplicateBall(Context context, float x, float y, float length, float height) {
        super(context, x, y, length, height);
        setAction("ball");
        setBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.power_up_ball2));
    }

    @Override
    public ArrayList<Ball> action(Context context, ArrayList<Ball> balls){
        Ball temp_ball = new Ball(context, balls.get(0).getX(), balls.get(0).getY(),
                balls.get(0).getLength(), balls.get(0).getObectHeight());
        temp_ball.oppositeXSpeed();
        balls.add(temp_ball);
        return balls;
    }

}
