package com.example.android.arkanoid.game.powerUp;

import android.content.Context;

import com.example.android.arkanoid.game.Brick;

/**
 * It is involved in creation of the Power Up statically
 */
public class PowerUpFactory {

    /**
     * Creates an Object {@link AbstractPowerUP}
     * Is is generated randomly
     *
     * @param context
     * @param brick
     * @return an {@link AbstractPowerUP} Object
     */
    public static AbstractPowerUP getRandomPowerUp(Context context, Brick brick){
        int choice = (int) (Math.random() * 20)+1;
        switch (choice) {
            case 1: {
                return new PowerUP_Lives(context, brick.getX(), brick.getY(),
                        brick.getLength(),brick.getObectHeight());

            }
            case 2:{
                return new PowerUP_DuplicateBall(context, brick.getX(), brick.getY(),
                        brick.getLength(),brick.getObectHeight());

            }
            case 3:{
                return new PowerUP_Wall(context, brick.getX(), brick.getY(),
                        brick.getLength(),brick.getObectHeight());

            }
            case 4:{
                return new PowerUP_IncreasePaddleLength(context, brick.getX(), brick.getY(),
                        brick.getLength(),brick.getObectHeight());

            }
            case 5:{
                return new PowerUP_DecreasePaddleLength(context, brick.getX(), brick.getY(),
                        brick.getLength(),brick.getObectHeight());

            }
            case 6:{
                return new PowerUP_Magnet(context, brick.getX(), brick.getY(),
                        brick.getLength(),brick.getObectHeight());
            }

            default:{
                return null;
            }
        }
    }



}
