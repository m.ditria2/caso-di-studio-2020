package com.example.android.arkanoid.recyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.arkanoid.R;

import java.util.ArrayList;

/**
 * Creates a {@link RecyclerView} which handles the global Rank of the game
 * @see com.example.android.arkanoid.ScoreActivity
 */
public class ContactsRecViewAdapter extends RecyclerView.Adapter<ContactsRecViewAdapter.ViewHolder> {

    private ArrayList<Contact> contacts = new ArrayList<>();
    public ContactsRecViewAdapter(){

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.contacts_list_item, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.txtPos.setText(String.valueOf(contacts.get(i).getPos()));
        viewHolder.txtName.setText(contacts.get(i).getName());
        viewHolder.txtScore.setText(String.valueOf(contacts.get(i).getScore()));
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView txtName, txtScore, txtPos;

        public ViewHolder(@NonNull View itemView){
            super(itemView);
            txtPos = itemView.findViewById(R.id.txtPosition);
            txtName = itemView.findViewById(R.id.txtName);
            txtScore = itemView.findViewById(R.id.txtScore);
        }
    }
}
