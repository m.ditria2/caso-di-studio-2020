package com.example.android.arkanoid;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Display;

import com.example.android.arkanoid.game.MultiGame;
import com.example.android.arkanoid.game.UpdateThread;
import com.example.android.arkanoid.preferences.PreferenceHelper;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MultiGameActivity extends AppCompatActivity {
    private String roomName ="";
    private MultiGame game;
    private final int[] bricksArr ={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
    private UpdateThread myThread;
    private Handler updateHandler;
    private int result=-1;
    Display display;
    Point size = new Point();

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef, myRefEnemy,myGameResult;
    String name, enemy;
    float enemyPaddle, enemyBallX, enemyBallY,enemySpeedX,enemySpeedY;
    int enemyScreenX, enemyScreenY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        display = getWindowManager().getDefaultDisplay();
        display.getSize(size);
        //sets the screen orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // create a new game
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        roomName = bundle.getString("roomName");
        name = bundle.getString("player").equals("player1") ? "user1" : "user2";
        enemy = bundle.getString("player").equals("player1") ? "user2" : "user1";
        game = new MultiGame(getApplicationContext(),name,roomName);
        setContentView(game);

        //create a handler and a thread
        createHandler();
        myThread = new UpdateThread(updateHandler);
        myThread.start();
        myRef = database.getReference("rooms/" + roomName + "/game/" + name );
        myRefEnemy = database.getReference("rooms/" + roomName + "/game/" + enemy );
        myRef.child("screen_x").setValue(size.x);
        myRef.child("screen_y").setValue(size.y);
        myGameResult = database.getReference("rooms/" + roomName + "/result");

        if(name.equals("user1")){
            myRefEnemy.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.child("paddle").getValue()!=null){
                        enemyPaddle = dataSnapshot.child("paddle").getValue(Float.class);
                    }
                    if(dataSnapshot.child("screen_x").getValue()!=null){
                        enemyScreenX = dataSnapshot.child("screen_x").getValue(Integer.class);
                    }
                    if(dataSnapshot.child("screen_y").getValue()!=null) {
                        enemyScreenY = dataSnapshot.child("screen_y").getValue(Integer.class);
                    }
                }
                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    Log.w("TAG", "Failed to read value.", error.toException());
                }
            });
            myGameResult.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if(snapshot.getValue()!=null){
                    result = snapshot.getValue(Integer.class);
                    if(result==0){
                        result = 1;
                    }else if(result == 1){
                        result = 0;
                    }
                        result();
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

        }else{
            myRefEnemy.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.child("paddle").getValue()!=null){
                        enemyPaddle = dataSnapshot.child("paddle").getValue(Float.class);
                    }
                    if(dataSnapshot.child("ball_x").getValue()!=null) {
                        enemyBallX = dataSnapshot.child("ball_x").getValue(Float.class);
                    }
                    if(dataSnapshot.child("ball_y").getValue()!=null) {
                        enemyBallY = dataSnapshot.child("ball_y").getValue(Float.class);
                    }
                    if(dataSnapshot.child("screen_x").getValue()!=null){
                        enemyScreenX = dataSnapshot.child("screen_x").getValue(Integer.class);
                    }
                    if(dataSnapshot.child("screen_y").getValue()!=null) {
                        enemyScreenY = dataSnapshot.child("screen_y").getValue(Integer.class);
                    }
                    if (dataSnapshot.child("speedX").getValue() != null) {
                        enemySpeedX = dataSnapshot.child("speedX").getValue(Integer.class);
                    }
                    if (dataSnapshot.child("speedY").getValue() != null) {
                        enemySpeedY = dataSnapshot.child("speedY").getValue(Integer.class);
                    }
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    Log.w("TAG", "Failed to read value.", error.toException());
                }
            });

            myRefEnemy.child("bricks").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    for(int i=0;i<24;i++) {
                        if (snapshot.child("brick" + i).getValue() != null) {
                            bricksArr[i] = snapshot.child("brick" + i).getValue(Integer.class);
                        }
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                }
            });

            myGameResult.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if(snapshot.getValue()!=null){
                        result = snapshot.getValue(Integer.class);
                        result();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

        }
    }


    private void result(){
        if(result == 0){
            game.activatePopupWindow(getResources().getString(R.string.you_lose));
        }else if(result == 1){
            game.activatePopupWindow(getResources().getString(R.string.you_win));
        }else if(result == 2){
            game.activatePopupWindow(getResources().getString(R.string.you_win));
        }
    }

    @SuppressLint("HandlerLeak")
    private void createHandler() {
        updateHandler = new Handler() {
            public void handleMessage(Message msg) {
                game.invalidate();
                game.update(bricksArr,enemyPaddle,enemyScreenX,enemyScreenY,enemyBallX,enemyBallY,enemySpeedX,enemySpeedY);
                super.handleMessage(msg);
            }
        };
    }

    protected void onPause() {
        super.onPause();
        closeActivity();
    }

    protected void onResume() {
        super.onResume();

        if(FirebaseDatabase.getInstance()!=null) {
            FirebaseDatabase.getInstance().goOnline();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        myThread.stopThread();
        myGameResult.removeValue();
        database.getReference("rooms/" +roomName + "/game").removeValue();
        database.getReference("rooms/" +roomName + "/player1").removeValue();
        database.getReference("rooms/" +roomName + "/player2").removeValue();
    }

    @Override
    public void onBackPressed(){
        closeActivity();
    }

    private void closeActivity() {
        if(result != 0 && result != 1) {
            myGameResult.setValue(2);
        }
        myThread.stopThread();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}