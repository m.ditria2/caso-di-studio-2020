package com.example.android.arkanoid.game;

/**
 * This class manages the creation of the appropriate level
 * @see Game
 */
public class EditorLevel {
    /**
     * Create the level requested
     *
     * @param level: number of the level that has to be create
     * @return
     */
    public int[][] createLevel(int level){
        int[][] bricksLevel;
        switch (level) {
            case 0: {
                bricksLevel = makeLevel0();
                break;
            }
            case 1: {
                bricksLevel = makeLevel1();
                break;
            }
            case 2: {
                bricksLevel = makeLevel2();
                break;
            }
            case 3: {
                bricksLevel = makeLevel3();
                break;
            }
            case 4: {
                bricksLevel = makeLevel4();
                break;
            }
            case 5: {
                bricksLevel = makeLevel5();
                break;
            }
            default: {
                bricksLevel = null;
                break;
            }
        }
        return bricksLevel;
    }



    public int[][] makeLevel0() {
        int[][] bricks = {
                {2,0,2,1,2,0,2},
                {1,0,1,2,1,0,1},
                {0},
                {2,0,2,1,2,0,2},
                {1,0,1,2,1,0,1}};
        return bricks;
    }

    public int[][] makeLevel1() {
        int[][] bricks = new int[5][7];

        for (int i = 0; i < bricks.length; i++) {
            for (int j = 0; j < bricks[i].length; j++) {
                if(i%2 == 1 && j%2 == 0) {
                    bricks[i][j] = 2;
                } else if (i%2 == 0 && j%2 == 1) {
                    bricks[i][j] = 1;
                } else {
                    bricks[i][j] = 0;
                }
            }
        }
        return bricks;
    }


    public int[][] makeLevel2() {
        int[][] bricks = {
                {3,3,3,0,3,3,3},
                {2,2,2,0,2,2,2},
                {1,1,1,0,1,1,1,},
                {0},
                {0,0,3,3,3,0,0},
                {0,0,2,2,2,0,0},
                {0,0,1,1,1,0,0}};
        return bricks;
    }

    public int[][] makeLevel3() {
        int[][] bricks = {
                {-1,0,0,0,0,0,-1},
                {0,1,0,0,0,1,0},
                {0,0,3,0,3,0,0},
                {0,1,0,-1,0,1,0},
                {0,0,3,0,3,0,0},
                {0,1,0,0,0,1,0},
                {1,0,0,0,0,0,1},
                {3,3,2,-1,2,3,3}};
        return bricks;
    }


    public int[][] makeLevel4() {
        int[][] bricks = {
                {1,-1,3,1,3,-1,1},
                {3,2,1,1,1,2,3},
                {1,1,2,-1,2,1,1},
                {0,1,3,3,3,1,0},
                {0},
                {2,2,1,1,1,2,2}};
        return bricks;
    }



    public int[][] makeLevel5() {
        int[][] bricks = {
                {3,3,-1,1,-1,3,3},
                {0},
                {1,1,1,0,1,1,1},
                {1,1,0,0,0,1,1,},
                {0},
                {1,-1,3,3,3,-1,1},
                {0,0,2,2,2,0,0},
                {0,0,2,2,2,0,0}};

        return bricks;
    }


}




