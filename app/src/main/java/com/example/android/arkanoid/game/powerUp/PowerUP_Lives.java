package com.example.android.arkanoid.game.powerUp;

import android.content.Context;
import android.graphics.BitmapFactory;

import com.example.android.arkanoid.R;

public class PowerUP_Lives extends AbstractPowerUP {

    private int lives;

    public PowerUP_Lives(Context context, float x, float y, float length, float height) {
        super(context, x, y, length, height);
        setBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.power_up_life2));
        setAction("life");
    }

    @Override
    public int action(int lives)
    {
        this.lives = lives;
        if(this.lives  < 3){
            this.lives++;
        }
        return this.lives;
    }

}
