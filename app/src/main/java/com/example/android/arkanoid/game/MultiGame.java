package com.example.android.arkanoid.game;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.example.android.arkanoid.MainActivity;
import com.example.android.arkanoid.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * This class manages the dynamics of the game in Multiplayer mode.
 * It calls the onDraw method, update method and
 * both the SensorManager and OnTouchEvent.
 * Each one of them is called every frame of the Game.
 * The speed of execution is defined in {@link UpdateThread}
 *
 * It links all the components of the Game
 * and makes them work together.
 *
 * {@link com.example.android.arkanoid.MultiGameActivity} support this class
 * and inistantiate it
 *
 * @see View
 * @see SensorEventListener
 * @see View.OnTouchListener
 * @see com.example.android.arkanoid.MultiGameActivity
 */
public class MultiGame extends View implements SensorEventListener, View.OnTouchListener {

    private final Context context;

    private final Ball ball;
    private final float paddleLength;
    private final float paddleHeight;
    private final float ballHeight;
    private final float ballLength;
    private final float brickHeight;
    private final float brickLength;
    private final float objectLength;
    private RectF rectF;
    private final Paint paint;
    private final Paddle paddle1;
    private final Paddle paddle2;
    private Bitmap background;
    private Bitmap strechSize;
    private final Bitmap paddle_p;
    private Display display;
    private Point size;
    private int screenX, screenY;
    private boolean popupWindow=false;
    private final Brick[] bricks1;
    private final Brick[] bricks2;
    private boolean start;
    private int brokenBrick =-1;
    private float tempBallX,tempBallY;
    private final String player;
    private final String roomName;
    private String result;
    private int cuntDown=5;
    private float ratioX,ratioY,tempEnemySpeedX;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef,myGameResult;

    /**
     * Called only once in the game (the first time
     * we enter the {@link android.app.Activity} calling it)
     *
     * @see com.example.android.arkanoid.MultiGameActivity
     *
     * @param context
     * @param player: possible values: "user1"/"user2"
     * @param roomName
     */
    public MultiGame(Context context, String player,String roomName) {
        super(context);
        myRef = database.getReference("rooms/" + roomName + "/game/" + player );
        myGameResult = database.getReference("rooms/" + roomName + "/result");
        paint = new Paint();

        this.player = player;
        this.roomName = roomName;
        breakoutView(context);

        paddle_p = BitmapFactory.decodeResource(getResources(), R.drawable.paddle_2);

        ballHeight = screenX / 22;
        ballLength = screenY / (screenY / (ballHeight));
        paddleLength = screenX / 4;
        paddleHeight = ballHeight;

        bricks1 = new Brick[12];
        bricks2 = new Brick[12];
        paddle1 = new Paddle(context, (screenX / 2) - (paddleLength / 2), screenY/2+screenY/3+screenY/18, paddleLength, paddleHeight);
        paddle2 = new Paddle(context, (screenX / 2) - (paddleLength / 2),screenY/2-screenY/3-screenY/13, paddleLength, paddleHeight);

        objectLength = screenX / 4;
        brickHeight = paddleHeight;
        brickLength = objectLength / 3;
        createBricks(context);
        ball = new Ball(context, (screenX / 2) - (ballLength / 2), screenY / 2 - ballHeight / 2,ballHeight, ballLength,20);
        this.context = context;

        new CountDownTimer(5000, 1000) {

            public void onTick(long millisUntilFinished) {
                cuntDown--;
            }

            public void onFinish() {
                cuntDown = -1;
                start = true;
            }
        }.start();

    }

    /**
     * Sets the background and screen display variables
     * @param context
     */
    private void breakoutView(Context context) {
        background = Bitmap.createBitmap(BitmapFactory.decodeResource(this.getResources(), R.drawable.sfondo_game4));
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = wm.getDefaultDisplay();
        size = new Point();
        display.getSize(size);

        //set the point (X,Y) to the size of the screen
        screenX = size.x;
        screenY = size.y;
    }


    /**
     * called each frame of the game: makes all the logic.
     * It makes each component having speed to move
     * and checks all the collision
     *
     * The parameters passed are read from Firebase Realtime Database
     * in {@link com.example.android.arkanoid.MultiGameActivity} and
     * represents the enemy variables (these parameters are used only if
     * we are playing as user2)
     *
     * The user1 host the game
     *
     * @param enemyBricks
     * @param enemyPaddle
     * @param enemyscreenX
     * @param enemyscreenY
     * @param enemyBallX
     * @param enemyBallY
     * @param enemySpeedX
     * @param enemySpeedY
     *
     */
    public void update(int[] enemyBricks, float enemyPaddle, int enemyscreenX, int enemyscreenY, float enemyBallX,float enemyBallY, float enemySpeedX, float enemySpeedY) {
        ratioX = ((float)(screenX)/(float)(enemyscreenX));
        ratioY = ((float)(screenY)/(float)(enemyscreenY));
        if (start) {
            paddle2.setX(screenX-(enemyPaddle* ratioX)-paddleLength);

            if (player.equals("user1")) {

                ball.isNearToPaddleMulti(paddle2.getX(), paddle2.getY(), paddleLength, paddleHeight);
                ball.isNearToPaddleMulti(paddle1.getX(), paddle1.getY(), paddleLength, paddleHeight);
                checkEdges();
                setbricks();
                ball.shift();
                float tempBX = ball.getX()+ball.getxSpeed()*2;
                float tempBY = ball.getY()+ball.getySpeed()*2;
                myRef.child("paddle").setValue(paddle1.getX());
                myRef.child("ball_x").setValue(tempBX);
                myRef.child("ball_y").setValue(tempBY);

            }
            else {

                if(enemyscreenX != 0 && enemyscreenY != 0){
                    if( Math.abs(tempBallX- enemyBallX)>0 && Math.abs(tempBallY- enemyBallY)>0)
                    {
                        tempBallX=(enemyBallX* ratioX)+(ballLength*ratioX);
                        tempBallY=(enemyBallY* ratioY)+(ballHeight*ratioY);
                        ball.setX(screenX-tempBallX);
                        ball.setY(screenY-tempBallY);
                        tempBallX = enemyBallX;
                        tempBallY = enemyBallY;
                        tempEnemySpeedX = enemySpeedX;
                    }
                    else{
                        ball.setX(ball.getX() - (enemySpeedX * ratioX));
                        ball.setY(ball.getY() - (enemySpeedY * ratioY));
                    }
                }

                ball.isNearToPaddleMulti(paddle2.getX() * ratioX, paddle2.getY() * ratioY, paddleLength, paddleHeight);
                ball.isNearToPaddleMulti(paddle1.getX() * ratioX, paddle1.getY() * ratioY, paddleLength, paddleHeight);
                checkEdgesMulti();
                getBricks(enemyBricks);
                myRef.child("paddle").setValue(paddle1.getX());
            }
        }
    }

    /**
     * Used by the user1
     * Sets all the bricks on the screen
     */
    private  void setbricks(){

        for (int i = 0; i < bricks1.length; i++) {
            Brick brick = bricks1[i];
            if (ball.isTouchingBrick(brick, bricks1,i)) {
                brokenBrick = i;
                myRef.child("bricks").child("brick"+ brokenBrick).setValue(0);
            }
        }

        for (int i = 0; i < bricks2.length; i++) {
            Brick brick = bricks2[i];
            if (ball.isTouchingBrick(brick, bricks2,i)) {
                brokenBrick = i+ bricks1.length;
                myRef.child("bricks").child("brick"+ brokenBrick).setValue(0);
            }
        }
    }

    /**
     * Used by the user2
     * Sets the bricks on the screen read from Firebase
     *
     * @param enemyBricks
     */
    private  void getBricks(int[] enemyBricks){

        for (int i = 0; i < bricks1.length; i++) {
            if (enemyBricks[i] == 0) {
                bricks2[bricks1.length-1-i]=null;
            }
        }

        for (int i = bricks1.length; i < bricks1.length+ bricks2.length; i++) {
            if (enemyBricks[i] == 0) {
                bricks1[bricks2.length+ bricks1.length-1-i]=null;
            }
        }
    }

    /**
     * Once the game is finished it comes back to {@link MainActivity}
     * Activated in {@link #onTouchEvent(MotionEvent)}
     */
    public void returnMenu(){
        myGameResult.setValue(2);
        start=false;
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        database.getReference("rooms/" +roomName + "/game").removeValue();
        if(player.equals("user1")){
            database.getReference("rooms/" +roomName + "/player1").removeValue();
        }
        else{
            database.getReference("rooms/" +roomName + "/player2").removeValue();
        }
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getContext().startActivity(intent);
    }

    /**
      * It captures all the touching events:
     * - ACTION_DOWN: called when the player touches the screen
     * - ACTION_MOVE: it starts when the player moves the finger on the screen
     * - ACTION_UP: called when the interaction ends and the finger is lifted up from the screen
     *
     * @param event
     * @return
     *
     * @see View.OnTouchListener
     */
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                if(popupWindow==true) {
                    returnMenu();
                }
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                if(start==false) {
                }else{
                    paddleMoves(event);
                }
                break;
            }
            case MotionEvent.ACTION_UP:
                break;
        }

        return true;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    /**
     * Called each frame of the game: draws all the objects on the screen
     * @param canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (strechSize == null) {
            strechSize = Bitmap.createScaledBitmap(background, size.x, size.y, false);
        }
        canvas.drawBitmap(strechSize, 0, 0, paint);

        paint.setColor(Color.WHITE);
        rectF = new RectF(paddle1.getX(), paddle1.getY(), paddle1.getX() + paddleLength, paddle1.getY() + paddleHeight);
        canvas.drawBitmap(paddle_p, null, rectF, paint);

        rectF = new RectF(paddle2.getX(), paddle2.getY(), paddle2.getX() + paddleLength, paddle2.getY() + paddleHeight);
        Bitmap paddle_p2 = rotateBitmap(paddle_p, 180);
        canvas.drawBitmap(paddle_p2, null, rectF, paint);

        paint.setColor(Color.GREEN);
        for (int i = 0; i < bricks2.length; i++) {
           if(bricks2[i] != null){
               Brick b = bricks2[i];
               rectF = new RectF(b.getX(), b.getY(), b.getX() + (brickLength), b.getY() + (brickHeight));
               canvas.drawBitmap(b.getBitmap(), null, rectF, paint);
           }
        }

            for (int i = 0; i < bricks1.length; i++) {
                if(bricks1[i] != null) {
                    Brick b = bricks1[i];
                    rectF = new RectF(b.getX(), b.getY(), b.getX() + (brickLength), b.getY() + (brickHeight));
                    canvas.drawBitmap(b.getBitmap(), null, rectF, paint);
                }
            }

        paint.setColor(Color.RED);
        canvas.drawBitmap(ball.getBitmap(), ball.getX(), ball.getY(), paint);

        if(popupWindow){
            paint.setColor(Color.WHITE);
            paint.setTextAlign(Paint.Align.CENTER);
            paint.setTextSize(70);
            canvas.drawText(result,screenX/2,screenY/2,paint);
        }

        if(cuntDown<5 && cuntDown>0){
            paint.setColor(Color.WHITE);
            paint.setTextAlign(Paint.Align.CENTER);
            paint.setTextSize(400);
            canvas.drawText(String.valueOf(cuntDown),screenX/2 ,screenY/2,paint);
        }

    }

    /**
     * Used to make the enemy paddle appear reversed
     * @param source
     * @param angle
     * @return
     */
     public static Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    /**
     * Moves the enemy paddle to the position read from Firebase
     * @param event
     */
    public void paddleMoves(MotionEvent event) {
        if (event.getX() > screenX - paddleLength / 2) {
            paddle1.setX(screenX - paddleLength);
        } else if (event.getX() < paddleLength / 2) {
            paddle1.setX(0);
        } else {
            paddle1.setX(event.getX() - paddleLength / 2);
        }
    }

    /**
     * fills up the list with bricks
     * @param context
     */
    private void createBricks(Context context) {
            for (int j = 0; j < (int)(screenX/(brickLength)); j++) {
                bricks2[j]=new Brick(context, j * (brickLength+1), paddle1.getY()+brickHeight, brickLength, brickHeight, 1);
            }
            for (int j = 0; j < (int)(screenX/(brickLength)); j++) {
                bricks1[j]=new Brick(context, j * (brickLength+1), paddle2.getY()-brickHeight, brickLength, brickHeight, 1);
            }
    }


    /**
     * Used as user2 to change the speed of the ball when
     * user1's ball touches an edge
     */
    private void checkEdgesMulti() {
        if (ball.getX() >= screenX - ballLength) {
            tempEnemySpeedX = -tempEnemySpeedX;

        } else if (ball.getX() <= 0) {
            tempEnemySpeedX = -tempEnemySpeedX;

        }
    }

    /**
     * Called each frame, it controls if a ball is touching an edge
     * of the screen. For the bottom edge the player loses a life
     */
    private boolean checkEdges() {
        if (ball.getX() >= screenX - ballLength) {
            ball.changeDirection("right");
            return true;
        } else if (ball.getX() <= 0) {
            ball.changeDirection("left");
            return true;
        } else if (ball.getY() <= 0) {
            // YOU WIN
            activatePopupWindow(getResources().getString(R.string.you_win));
            myGameResult.setValue(0);
            return true;
        }else if (ball.getY() >= screenY - (ballHeight)) {
            // YOU LOSE
            activatePopupWindow(getResources().getString(R.string.you_lose));
            myGameResult.setValue(1);
            return true;
        }
        return false;
    }

    public void activatePopupWindow(String esito){
        Log.d("TAG", "activatePopupWindow: " + esito);
        result = esito;
        start = false;
        popupWindow = true;
    }
}
