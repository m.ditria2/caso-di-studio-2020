package com.example.android.arkanoid.preferences;

import android.media.MediaPlayer;

/**
 * This class create an Object {@link MediaPlayer} linked with its {@link #currentLength}
 * It makes easier to pause and resume a sound (in particular background sounds)
 */
public class Sound {
    private MediaPlayer sound;
    private int currentLength;

    /**
     * Creates a sound and makes it play over and over (setLooping(true))
     *
     * @param sound: the resource to be played
     */
    public Sound(MediaPlayer sound) {
        this.sound = sound;

        sound.setLooping(true);
        sound.start();
    }

    public Sound(MediaPlayer sound, int currentLength) {
        this.sound = sound;
        this.currentLength = currentLength;
    }

    public void pause() {
        sound.pause();
        currentLength=sound.getCurrentPosition();
    }

    public void resume() {
        sound.seekTo(currentLength);
        sound.start();
    }

    public void release() {
        sound.release();
    }

    public void stop() {
        sound.stop();
    }
}
