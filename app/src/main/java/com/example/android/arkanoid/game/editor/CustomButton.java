package com.example.android.arkanoid.game.editor;

import android.view.View;
import android.widget.Button;
import com.example.android.arkanoid.R;
import java.io.Serializable;

/**
 * @see com.example.android.arkanoid.EditLevelActivity
 *
 * Used to set the buttons matrix in {@link com.example.android.arkanoid.EditLevelActivity}
 *
 *
 */
public class CustomButton implements Serializable {
    private Button button;
    public int resistence;

    public CustomButton(Button button, int resistence) {
        this.button = button;
        this.resistence = resistence;
        setListener();
    }

    /**
     *  This class change the background and the resistence
     *  based on the click of the user
     */
    public void setListener() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resistence++;
                switch (resistence) {
                    case 0: {
                        button.setBackgroundResource(android.R.drawable.btn_default);
                        break;
                    }
                    case 1: {
                        button.setBackgroundResource(R.drawable.brick_verde);
                        break;
                    }
                    case 2: {
                        button.setBackgroundResource(R.drawable.bricks_giallo);
                        break;
                    }
                    case 3: {
                        button.setBackgroundResource(R.drawable.brick_viola);
                        break;
                    }
                    default:{
                        resistence = -1;
                        button.setBackgroundResource(R.drawable.brick_metal);
                        break;
                    }
                }

            }
        });
    }

    public Button getButton() {
        return button;
    }

    public void setButton(Button button) {
        this.button = button;
    }

    public int getResistence() {
        return resistence;
    }

    @Override
    public String toString() {
        return "CustomButton{" +
                "button=" + button +
                ", resistence=" + resistence +
                '}';
    }
}
