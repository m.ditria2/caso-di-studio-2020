package com.example.android.arkanoid;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.android.arkanoid.databaseHelper.DatabaseHelper;
import com.example.android.arkanoid.formWindowHelper.FormWindow;
import com.example.android.arkanoid.multiplayer.LoginActivity;
import com.example.android.arkanoid.preferences.PreferenceHelper;
import com.example.android.arkanoid.preferences.SoundHelper;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    Button btnSingleplayer, btnMultiplayer, btnExit, btnSound;

    private DatabaseHelper db;
    private SQLiteDatabase database;

    FirebaseDatabase firebase;

    private DialogInterface.OnClickListener dialogClickListener;

    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setAppLocale();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSingleplayer = findViewById(R.id.btnSingleplayer);
        btnMultiplayer = findViewById(R.id.btnMultiplayer);
        btnExit = findViewById(R.id.btnExit);
        btnSound = findViewById(R.id.btnSound);

        db = new DatabaseHelper(this);
        database = db.getWritableDatabase();
        SoundHelper.pauseAll();

        SoundHelper.setSound(this,R.raw.suono_home);

        firebase = FirebaseDatabase.getInstance();
        
        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        if (PreferenceHelper.isLogged(this)) {
            myToolbar.setTitle(PreferenceHelper.getUserLogged(this));
        }

        if(PreferenceHelper.getPreferenceController(getApplicationContext()).equals("empty") ||
            PreferenceHelper.getPreferenceController(getApplicationContext()).isEmpty()) {
            PreferenceHelper.setPreferenceController("touchpad",getApplicationContext());
        }

        setSoundButton();

        dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        finishAndRemoveTask();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        btnSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PreferenceHelper.isSoundPrefOff(getApplicationContext())) {
                    PreferenceHelper.setSoundPref("on",getApplicationContext());
                    SoundHelper.setSound(getApplicationContext(),R.raw.suono_home);
                } else{
                    SoundHelper.pauseAll();
                    PreferenceHelper.setSoundPref("off",getApplicationContext());
                }
                setSoundButton();
            }
        });

        btnSingleplayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SinglePlayerActivity.class);
                startActivity(intent);
            }
        });
        btnMultiplayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkConnected()){
                    if(PreferenceHelper.isLogged(MainActivity.this)){
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(intent);
                    }else{
                        Toast.makeText(MainActivity.this, getResources().getString(R.string.login_multiplayer), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(MainActivity.this, getResources().getString(R.string.no_connection_multiplayer), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDialog();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        this.menu = menu;
        MenuItem itemSignup = menu.findItem(R.id.itemSignup);
        MenuItem itemLogin = menu.findItem(R.id.itemLogin);
        MenuItem itemLogout = menu.findItem(R.id.itemLogout);
        if (PreferenceHelper.isLogged(getApplicationContext())) {
            itemSignup.setVisible(false);
            itemLogin.setVisible(false);
            itemLogout.setVisible(true);
        } else {
            itemSignup.setVisible(true);
            itemLogin.setVisible(true);
            itemLogout.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itemLogin:{
                if (FormWindow.window == null) {
                    FormWindow.createWindow("login", this);
                    FormWindow.window.show();
                }
                break;
            }

            case R.id.itemSignup: {
                if (FormWindow.window == null) {
                    FormWindow.createWindow("signup", this);
                    FormWindow.window.show();
                }
                break;
            }

            case R.id.itemLogout: {
                PreferenceHelper.deleteUserPref(getApplicationContext());
                FormWindow.reload(this);
                break;
            }

            case R.id.itemSettings: {
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void setAppLocale() {
        String localeCode = PreferenceHelper.getPreferenceLanguage(getApplicationContext());
        Resources resources = getBaseContext().getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(new Locale(localeCode.toLowerCase()));
        } else {
            configuration.locale = new Locale(localeCode.toLowerCase());
        }
        resources.updateConfiguration(configuration, displayMetrics);
    }



    @Override
    protected void onPause() {
        super.onPause();
        if(FormWindow.window != null){
            if (FormWindow.window.isShowing()) {
                FormWindow.window.dismiss();
                FormWindow.window = null;
            }
        }
        SoundHelper.pause(R.raw.suono_home);
        setSoundButton();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!PreferenceHelper.isSoundPrefOff(this)) {
            SoundHelper.resume(R.raw.suono_home);
        }
       setSoundButton();
    }

    public void setSoundButton(){
        if(PreferenceHelper.isSoundPrefOff(this)){
            btnSound.setBackground(getDrawable(R.drawable.sound_off));
        } else {
            btnSound.setBackground(getDrawable(R.drawable.sound_on));
        }
    }

    @Override
    public void onBackPressed() {
        showDialog();
    }

    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this,R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        builder.setMessage(this.getResources().getString(R.string.are_you_sure)).setPositiveButton(this.getResources().getString(R.string.yes), dialogClickListener).setNegativeButton( this.getResources().getString(R.string.no), dialogClickListener).show();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}
