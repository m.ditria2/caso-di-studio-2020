package com.example.android.arkanoid;

/*import android.support.v7.app.AppCompatActivity;*/
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
/*import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;*/

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.arkanoid.preferences.PreferenceHelper;
import com.example.android.arkanoid.preferences.SoundHelper;
import com.example.android.arkanoid.recyclerView.Contact;
import com.example.android.arkanoid.recyclerView.ContactsRecViewAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

public class ScoreActivity extends AppCompatActivity {

    private Button btnShare;
    private RecyclerView contactsRecView;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference reference;
    private int count;
    String textUser;
    private ArrayList<Contact> contacts;
    private Contact myContact;
    private TextView txtPosition, txtName, txtScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        btnShare = findViewById(R.id.btnShare);
        textUser = getResources().getString(R.string.contact_prefix) + " ";

        contacts = new ArrayList<>();
        contactsRecView = findViewById(R.id.contactsRecView);

        txtPosition = findViewById(R.id.txtMyPosition);
        txtName = findViewById(R.id.txtMyName);
        txtScore = findViewById(R.id.txtMyScore);

        firebaseDatabase = FirebaseDatabase.getInstance();
        reference = firebaseDatabase.getReference("registeredUsers");

        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot temp : snapshot.getChildren()) {
                    contacts.add(new Contact(0,temp.child("username").getValue(String.class),
                            temp.child("score").getValue(Integer.class)));
                }

                setAdapter();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    public void setAdapter(){
        ContactsRecViewAdapter adapter = new ContactsRecViewAdapter();
        Collections.sort(contacts);
        for (Contact contact: contacts) {
            count++;
            contact.setPos(count);
            if(contact.getName().equals(PreferenceHelper.getUserLogged(this))) {
                myContact = contact;
                txtPosition.setText(String.valueOf(myContact.getPos()));
                txtName.setText(myContact.getName());
                txtScore.setText(String.valueOf(myContact.getScore()));
                textUser += myContact.toString(this);
                textUser +=  ". "  + getResources().getString(R.string.contact_suffix);
                textUser += "\nhttps://drive.google.com/file/d/1NyVUPS6pDb_RJmiHHZmOs958BMgcwT_D/view?usp=sharing";
            }
        }
        adapter.setContacts(contacts);

        contactsRecView.setAdapter(adapter);
        contactsRecView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND );
                sendIntent.putExtra(Intent.EXTRA_TEXT, textUser);
                sendIntent.setType("text/plain");

                Intent shareIntent = Intent.createChooser(sendIntent, null);
                startActivity(shareIntent);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        SoundHelper.pause(R.raw.suono_home);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!PreferenceHelper.isSoundPrefOff(this)) {
            SoundHelper.resume(R.raw.suono_home);
        }
    }

}