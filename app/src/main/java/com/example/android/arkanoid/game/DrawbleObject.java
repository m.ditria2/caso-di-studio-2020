package com.example.android.arkanoid.game;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;

/**
 * This class manages all the drawable objects in
 * {@link Game} and {@link MultiGame}
 *
 * In particular it specifies the Bitmap for each Object
 * and its coordinates
 */
public abstract class DrawbleObject extends View {

    private float x;
    private float y;

    private float length;
    private float height;

    private Bitmap bitmap;

    public DrawbleObject(Context context, float x, float y, float length, float height) {
        super(context);
        this.x = x;
        this.y = y;
        this.length = length;
        this.height = height;
    }

    @Override
    public void setX(float x) {
        this.x = x;
    }

    @Override
    public void setY(float y) {
        this.y = y;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    @Override
    public float getX() {
        return x;
    }

    @Override
    public float getY() {
        return y;
    }

    public float getLength() {
        return length;
    }

    public float getObectHeight() {
        return height;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
}
