package com.example.android.arkanoid.game.powerUp;

import android.content.Context;
import android.graphics.BitmapFactory;

import com.example.android.arkanoid.R;

public class PowerUP_Wall extends AbstractPowerUP {

    public PowerUP_Wall(Context context, float x, float y, float length, float height) {
        super(context, x, y, length, height);
        setAction("wall");
        setBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.power_up_wall2));
    }

}
