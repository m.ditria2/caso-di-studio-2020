package com.example.android.arkanoid.firebaseHelper;

/**
 * Class that helps the saving of the user on Firebase
 *
 */
public class RegisteredUser {
    private String username;
    private String password;
    private int score;

    public RegisteredUser(){}

    /**
     * Score is set to 0 once the user has been registered
     *
     * @param username
     * @param password
     */
    public RegisteredUser(String username, String password) {
        this.username = username;
        this.password = password;
        score = 0;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
