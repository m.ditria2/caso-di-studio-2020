package com.example.android.arkanoid.game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;

import com.example.android.arkanoid.R;
import com.example.android.arkanoid.game.powerUp.AbstractPowerUP;
import com.example.android.arkanoid.game.powerUp.PowerUpFactory;
import com.example.android.arkanoid.preferences.PreferenceHelper;

import java.util.ArrayList;

/**
 * The class Ball manages all the logic of the ball drawn down in the game
 */
public class Ball extends DrawbleObject {

    private float xSpeed;
    private float ySpeed;
    private float distanceToPaddle;
    private boolean ballOnMagnet = false;
    private float height;
    private float length;

    /**
     * Sets all the parameters
     * @see Game
     * @param context
     * @param x: coordinate x of the ball
     * @param y: coordinate y of the ball
     * @param length
     * @param height
     */
    public Ball(Context context, float x, float y, float length, float height) {
        super(context, x, y, length, height);
        this.height =  height;
        this.length = length;
        generateSpeed();
        setBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.redball3),(int)length,(int)height,false));
    }

    /**
     * Alternative constructor for {@link MultiGame}
     *
     * @param context
     * @param x: coordinate x of the ball
     * @param y: coordinate y of the ball
     * @param length
     * @param height
     * @param i
     */
    public Ball(Context context, float x, float y, float length, float height,int i) {
        super(context, x, y, length, height);
        this.height =  height;
        this.length = length;
        generateSpeedMulti(i);
        setBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.redball3),(int)length,(int)height,false));
    }

    /**
     * Alternative speed generator for {@link MultiGame}
     * @param i
     */
    protected  void generateSpeedMulti(int i){
        xSpeed = 0;
        ySpeed = i;
    }


    /**
     * Gives ball a speed in the {@link Game}
      */
    protected void generateSpeed() {
        int maxY;
        int minX = 0;
        int minY = 0;
        int rangeX = 0;
        int rangeY = 0;
        String difficulty = PreferenceHelper.getPreferenceDifficulty(getContext());

        switch (difficulty) {
            case "easy": {
                minY = -10;
                maxY = -13;
                rangeY = maxY - minY + 1;
                //1
                break;
            }

            case "medium": {
                minY = -15;
                maxY = -18;
                rangeY = maxY - minY + 1;
                //6
                break;
            }

            case "hard": {
                minY = -21;
                maxY = -24;
                rangeY = maxY - minY + 1;
                //5
                break;
            }

            default:{
                break;
            }
        }

            xSpeed = (int) (Math.random() * rangeX) + minX;
            ySpeed = (int) (Math.random() * rangeY) + minY;
    }


    /**
     * Changes direction depending on which wall is hit by the ball
     * @param wall
     */
    protected void changeDirection(String wall) {
        if (xSpeed >= 0 && ySpeed <= 0 && wall.equals("right")) {
            oppositeXSpeed();
        } else if (xSpeed >= 0 && ySpeed <= 0 && wall.equals("up")) {
            oppositeYSpeed();
        } else if (xSpeed <= 0 && ySpeed <= 0 && wall.equals("up")) {
            oppositeYSpeed();
        } else if (xSpeed <= 0 && ySpeed <= 0 && wall.equals("left")) {
            oppositeXSpeed();
        } else if (xSpeed <= 0 && ySpeed >= 0 && wall.equals("left")) {
            oppositeXSpeed();
        } else if (xSpeed >= 0 && ySpeed >= 0 && wall.equals("down")) {
            oppositeYSpeed();
        } else if (xSpeed >= 0 && ySpeed >= 0 && wall.equals("right")) {
            oppositeXSpeed();
        }
    }


    /**
     * Checks if the ball is close to the paddle
     * @see Game
     *
     * @param ax: coordinate y of the paddle
     * @param ay: coordinate y of the paddle
     * @param paddleLength
     * @param paddleHeight
     */
    protected void isNearToPaddle(float ax, float ay, float paddleLength, float paddleHeight) {
        if (ax <= this.getX() + getLength() && (ax + paddleLength) >= this.getX() && ay <= (this.getY() + getObectHeight()) && (ay + paddleHeight) >= this.getY()) {
            if(!PreferenceHelper.isSoundPrefOff(getContext())) {
                MediaPlayer mediaPlayer = MediaPlayer.create(getContext(), R.raw.rimbalzo_pallina);
                mediaPlayer.start();
            }

            xSpeed = ((getX()+(length/2))-(ax+(paddleLength/2)))/10;
            oppositeYSpeed();
        }
    }

    /**
     * Checks if the ball is close to the paddle
     * Alternative method for {@link MultiGame}
     *
     * @param ax: coordinate x for the paddle
     * @param ay: coordinate y for the paddle
     * @param paddleLength
     * @param paddleHeight
     */
    protected void isNearToPaddleMulti(float ax, float ay, float paddleLength, float paddleHeight) {
        if (ax <= this.getX() + getLength() && (ax + paddleLength) >= this.getX() && ay <= (this.getY() + getObectHeight()) && (ay + paddleHeight) >= this.getY()) {
            /*MediaPlayer mediaPlayer = MediaPlayer.create(getContext(), R.raw.rimbalzo_pallina);
            mediaPlayer.start();*/
            xSpeed = ((getX()+(length/2))-(ax+(paddleLength/2)))/12;
            ySpeed = -ySpeed;
        }
    }


    /**
     * Checks if the ball is near to paddle
     * when the {@link com.example.android.arkanoid.game.powerUp.PowerUP_Magnet} is active
     *
     * @param ax: coordinate x of the paddle
     * @param ay: coordinate y of the paddle
     * @param paddleLength
     * @param paddleHeight
     * @return
     */
    protected boolean isNearToPaddleMagnet(float ax, float ay, float paddleLength, float paddleHeight) {
        if (ax <= this.getX() + getLength() && (ax + paddleLength) >= this.getX() && ay <= (this.getY() + getObectHeight()  + 21) && (ay + paddleHeight) >= this.getY()) {
            if(ySpeed != 0) {
                distanceToPaddle = getX() - ax;
            }
            setBallOnMagnet(true);
            xSpeed =0;
            ySpeed =0;
            return true;
        }
        return false;
    }


    /**
     * Checks if the ball is near to a brick
     *
     * @param ax: coordinate x of the brick
     * @param ay: coordinate y of the brick
     * @param brickLength
     * @param brickHeight
     * @return
     */
    protected boolean isNearToBrick(float ax, float ay, float brickLength, float brickHeight) {
        boolean flag = false;

        if((getX()+xSpeed+length > ax && getX()+xSpeed<ax+brickLength) &&
                ((getY()+ySpeed<ay+brickHeight && getY()+ySpeed+height>ay) || (getY()<ay+brickHeight && getY()+height>ay))) {
            //touching from right and left
            if(getY()+(height/2)>ay && getY()+(height/2)<ay+brickHeight) {
                oppositeXSpeed();
                setX(getX()+xSpeed);
            }
            flag = true;

        }

        if((getY()+ySpeed<ay+brickHeight && getY()+ySpeed+height>ay) &&
                ((getX()+xSpeed+length > ax && getX()+xSpeed<ax+brickLength) || (getX()+length > ax && getX()<ax+brickLength))) {
            //touching up and down
            if(getX()+(length/2)>ax && getX()+(length/2)<ax+brickLength) {

                setY(getY()+ySpeed);
            }
            oppositeYSpeed();


            flag = true;
        }
        return flag;
    }

    /**
     * Checks if the ball is colliding with a Brick
     * Alternative method for Multigame
     * @see MultiGame
     *
     * @param brick: the brick touched
     * @param bricks: list of bricks
     * @param i: position of the brick in bricks
     * @return
     */
    public boolean isTouchingBrick(Brick brick, Brick[] bricks,int i){
        if(bricks[i]!=null){
            if (isNearToBrick(brick.getX(), brick.getY(), brick.getLength(),brick.getObectHeight())) {
                if(brick.isBroken() & brick != null) {
                    if(bricks.length !=0 )
                    {
                        bricks[i]=null;
                    }
                } else {
                    brick.reduceResistence();
                    brick.setBrickBitmap();
                }
                return true;
            }
            return false;
        }
        else return false;
    }

    /**
     * Makes the ball move: it moves by {@link #xSpeed} pixels
     */
    protected void shift() {
        setX(getX() + xSpeed);
        setY(getY() + ySpeed);
    }


    /**
     * Checks if the ball is colliding with a Brick
     *
     * @see Game
     *
     * @param context
     * @param brick: the brick touched
     * @param bricks: list of bricks
     * @param powerUp
     * @return
     */
    public boolean isTouchingBrick(Context context, Brick brick, ArrayList<Brick> bricks, ArrayList<AbstractPowerUP> powerUp){
        if (isNearToBrick(brick.getX(), brick.getY(), brick.getLength(),brick.getObectHeight())) {

            if(brick.isBroken() && brick != null) {
                    AbstractPowerUP tempPowerUp = PowerUpFactory.getRandomPowerUp(context,brick);
                    if (tempPowerUp != null) powerUp.add(tempPowerUp);
                if(bricks.size()!=0)
                {
                    if(!PreferenceHelper.isSoundPrefOff(getContext())) {
                        MediaPlayer mediaPlayer = MediaPlayer.create(getContext(), R.raw.brick);
                        mediaPlayer.start();
                    }
                    bricks.remove(brick);
                }
            } else {
                brick.reduceResistence();
                brick.setBrickBitmap();
            }
            return true;

        }
        else return false;
    }

    public void oppositeXSpeed() { xSpeed = -xSpeed;   }

    public void oppositeYSpeed() {
        ySpeed = -ySpeed;
    }

    public float getDistanceToPaddle()
    {
        return distanceToPaddle;
    }

    public boolean getBallOnMagnet() {
        return ballOnMagnet;
    }

    public void setBallOnMagnet(boolean ballOnMagnet) {
        this.ballOnMagnet = ballOnMagnet;
    }

    public float getxSpeed() {
        return xSpeed;
    }

    public float getySpeed() {
        return ySpeed;
    }

}
