package com.example.android.arkanoid.game;

import android.os.CountDownTimer;
import android.util.Log;

/**
 * This class set a Timer for timed power Ups
 */
public class TimerForPowerUP {

    private CountDownTimer countDownTimer;

    private boolean isTimerRunning;

    private long timeLeftInSeconds;

    private long startTimeInSeconds;

    /**
     * Creates the timer with {@link #startTimeInSeconds} of duration
     *
     * @param startTimeInSeconds: seconds of duration
     */
    public TimerForPowerUP(long startTimeInSeconds) {
        this.startTimeInSeconds = startTimeInSeconds *1000;
        this.timeLeftInSeconds = startTimeInSeconds *1000;
    }

    public void startTimer(){
        countDownTimer = new CountDownTimer(timeLeftInSeconds, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLeftInSeconds = millisUntilFinished;
            }

            @Override
            public void onFinish() {
                isTimerRunning = false;
            }
        }.start();
        isTimerRunning = true;
    }

    public void pauseTimer(){
        countDownTimer.cancel();
        isTimerRunning = false;
    }

    public void resetTimer(){
        timeLeftInSeconds = startTimeInSeconds;
        pauseTimer();
        startTimer();
    }


    public boolean isTimerRunning() {
        return isTimerRunning;
    }
}
