package com.example.android.arkanoid;


import android.content.Intent;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.RadioButton;
import android.widget.RadioGroup;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.android.arkanoid.formWindowHelper.FormWindow;
import com.example.android.arkanoid.preferences.PreferenceHelper;
import com.example.android.arkanoid.preferences.SoundHelper;

import java.util.Locale;

public class SettingsActivity extends AppCompatActivity {
    private final String ITALIAN = "it";
    private final String DEFAULT_LANGUAGE = "en";
    private final String GYROSCOPE_VALUE = "gyroscope";
    private final String TOUCHPAD_VALUE = "touchpad";

    private Menu menu;


    RadioButton rbItalian, rbEnglish, rbGyroscope, rbTouchPad;
    RadioGroup rgLanguage, rgController;
    Locale locale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        rbEnglish = findViewById(R.id.rbEnglish);
        rbItalian = findViewById(R.id.rbItaliano);
        rgLanguage = findViewById(R.id.rgLanguage);

        rgController = findViewById(R.id.rgController);
        rbGyroscope = findViewById(R.id.rbGyroscope);
        rbTouchPad = findViewById(R.id.rbTouchpad);

        setLocaleLanguage();
        setCheckedController();

        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        if (PreferenceHelper.isLogged(this)) {
            myToolbar.setTitle(PreferenceHelper.getUserLogged(this));
        }

        rgLanguage.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = group.findViewById(checkedId);

                switch(checkedRadioButton.getId()) {
                    case R.id.rbEnglish: {
                        setAppLocale(DEFAULT_LANGUAGE);
                        PreferenceHelper.setPreferenceLanguage(DEFAULT_LANGUAGE, getApplicationContext());
                        break;
                    }
                    case R.id.rbItaliano: {
                        setAppLocale(ITALIAN);
                        PreferenceHelper.setPreferenceLanguage(ITALIAN,getApplicationContext());
                        break;
                    }
                    default:{
                        setDefaultLanguageSystem();
                        break;
                    }
                }
            }
        });

        rgController.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = group.findViewById(checkedId);

                switch (checkedRadioButton.getId()) {
                    case R.id.rbGyroscope: {
                        PreferenceHelper.setPreferenceController(GYROSCOPE_VALUE, getApplicationContext());
                        break;
                    }
                    case R.id.rbTouchpad: {
                        PreferenceHelper.setPreferenceController(TOUCHPAD_VALUE, getApplicationContext());
                        break;
                    }
                }

                
            }
        });

    }


    private void setAppLocale(String localeCode){
        Resources resources = getBaseContext().getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            configuration.setLocale(new Locale(localeCode.toLowerCase()));
        } else {
            configuration.locale = new Locale(localeCode.toLowerCase());
        }

        resources.updateConfiguration(configuration, displayMetrics);
        finish();
        startActivity(getIntent());
    }

   public void onBackPressed(){
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    public void setCheckedController() {
        switch (PreferenceHelper.getPreferenceController(this)) {
            case TOUCHPAD_VALUE: {
                rbTouchPad.setChecked(true);
                break;
            }
            case GYROSCOPE_VALUE: {
                rbGyroscope.setChecked(true);
                break;
            }
            default: {

                break;
            }
        }
    }


    public void setLocaleLanguage() {

        String prefLanguage = PreferenceHelper.getPreferenceLanguage(this);
        switch (prefLanguage) {
            case "en": {
                rbEnglish.setChecked(true);
                break;
            }
            case "it":
            default: {
                rbItalian.setChecked(true);
                break;

            }
        }

    }

    public void setDefaultLanguageSystem(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locale = getResources().getConfiguration().getLocales()
                    .getFirstMatch(getResources().getAssets().getLocales());

        } else {
            locale = getResources().getConfiguration().locale;
        }

        if (locale.toString().equals("it_IT") || locale.toString().equals(ITALIAN)) {
            rbItalian.setChecked(true);
        } else {
            rbEnglish.setChecked(true);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        SoundHelper.pause(R.raw.suono_home);
        if(FormWindow.window != null){
            if (FormWindow.window.isShowing()) {
                FormWindow.window.dismiss();
                FormWindow.window = null;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!PreferenceHelper.isSoundPrefOff(this)) {
            SoundHelper.resume(R.raw.suono_home);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        this.menu = menu;
        MenuItem itemSignup = menu.findItem(R.id.itemSignup);
        MenuItem itemLogin = menu.findItem(R.id.itemLogin);
        MenuItem itemLogout = menu.findItem(R.id.itemLogout);
        MenuItem itemSettings = menu.findItem(R.id.itemSettings);
        if (PreferenceHelper.isLogged(getApplicationContext())) {
            itemSignup.setVisible(false);
            itemLogin.setVisible(false);
            itemLogout.setVisible(true);
            itemSettings.setVisible(false);
        } else {
            itemSignup.setVisible(true);
            itemLogin.setVisible(true);
            itemLogout.setVisible(false);
            itemSettings.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itemLogin:{
                if (FormWindow.window == null) {
                    FormWindow.createWindow("login", this);
                    FormWindow.window.show();
                }
                break;
            }

            case R.id.itemSignup: {
                if (FormWindow.window == null) {
                    FormWindow.createWindow("signup", this);
                    FormWindow.window.show();
                }
                break;
            }

            case R.id.itemLogout: {
                PreferenceHelper.deleteUserPref(getApplicationContext());
                FormWindow.reload(this);
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }


}