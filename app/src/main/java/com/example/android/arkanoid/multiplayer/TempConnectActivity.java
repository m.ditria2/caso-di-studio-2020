package com.example.android.arkanoid.multiplayer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.example.android.arkanoid.MultiGameActivity;
import com.example.android.arkanoid.R;
import com.example.android.arkanoid.preferences.PreferenceHelper;
import com.example.android.arkanoid.preferences.SoundHelper;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * TempConnectActivity has 2 different behaviours
 * 1- If the user has created a room, he enters in this activity
 *    and waits for the second player
 * 2- If the user is entering the room as the second player,
 *    whenever he is ready, he can start the game.
 *
 * @see RoomActivity
 *
 * */
public class TempConnectActivity extends AppCompatActivity {
    Button button2;

    String playerName = "";
    String roomName = "";
    String role = "";

    FirebaseDatabase database;
    DatabaseReference statusFirstPlayer,statusSecondPlayer;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp_connect);

        progressBar = findViewById(R.id.progressBar);
        button2 = findViewById(R.id.button2);
        button2.setEnabled(false);
        database = FirebaseDatabase.getInstance();
        playerName = PreferenceHelper.getUserLogged(this);


        Bundle extras = getIntent().getExtras();

        if(extras != null){
            roomName = extras.getString("roomName");
            statusFirstPlayer = database.getReference("rooms/" + roomName + "/player1/status");
            statusSecondPlayer =database.getReference("rooms/" + roomName + "/player2/status");
            if(!roomName.equals(playerName)){
                role = "player2";
            }
            else if(roomName.equals(playerName)){
                role = "player1";
                statusFirstPlayer.setValue("on");
            }
        }

            if(role.equals("player1")){
                button2.setText(getResources().getString(R.string.wait_for_your_opponent));
                progressBar.setVisibility(View.VISIBLE);

            }else {
                button2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        statusSecondPlayer.setValue("on");
                        Intent intent = new Intent(getApplicationContext(), MultiGameActivity.class);
                        intent.putExtra("roomName", roomName);
                        intent.putExtra("player", role);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        button2.setEnabled(false);
                    }
                });
                progressBar.setVisibility(View.GONE);
            }
        //listen for incoming messages
        addRoomEventListener();
    }

    private  void addRoomEventListener() {
        statusSecondPlayer.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot datasnapshot) {
                //message received
                if (datasnapshot.getValue() != null ) {
                    if(role.equals("player1") && datasnapshot.getValue(String.class).equals("on")){

                        Intent intent = new Intent(getApplicationContext(), MultiGameActivity.class);
                        intent.putExtra("roomName", roomName);
                        intent.putExtra("player", role);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        statusFirstPlayer.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot datasnapshot) {
                //message received
                if (datasnapshot.getValue() != null ) {
                    if(role.equals("player2") && datasnapshot.getValue(String.class).equals("on")){
                        button2.setEnabled(true);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                //error = retry
            }
        });
    }

    @Override
    public void onBackPressed(){
        statusSecondPlayer.removeValue();
        statusFirstPlayer.removeValue();
        super.onBackPressed();
    }
    @Override
    public void onPause(){
        super.onPause();
        SoundHelper.pause(R.raw.suono_home);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!PreferenceHelper.isSoundPrefOff(this)) {
            SoundHelper.resume(R.raw.suono_home);
        }
    }
}