package com.example.android.arkanoid.game.powerUp;

import android.content.Context;

import com.example.android.arkanoid.game.Ball;
import com.example.android.arkanoid.game.DrawbleObject;
import com.example.android.arkanoid.game.TimerForPowerUP;

import java.util.ArrayList;
import java.util.Objects;

/**
 * It constrols all the PowerUps' behaviour
 *
 * @see DrawbleObject
 */
public abstract class AbstractPowerUP extends DrawbleObject {

    private float ySpeed;
    private String action;
    private TimerForPowerUP timerForPowerUP;

    public AbstractPowerUP(Context context, float x, float y, float length, float height) {
        super(context, x, y, length, height);
        generateSpeed();
        timerForPowerUP = new TimerForPowerUP(12);
        timerForPowerUP.startTimer();
    }

    protected void generateSpeed() {
        int maxY = -14;
        int minY = -16;
        int rangeY = maxY - minY + 1;

        ySpeed = (int) (Math.random() * rangeY) + minY ;
    }

    public boolean isCloseToPaddle(float ax, float ay, float paddleLength) {

        if((ax - getLength())<= getX() && (ax + paddleLength) >= getX() && (ay - getObectHeight()) <= getY() && ay  >= getY()){
            return true;
        }
        return false;
    }

    public void shift() {
        setY(getY() - ySpeed);
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int action(int lives){
        return lives;
    }

    public ArrayList<Ball> action(Context context, ArrayList<Ball> balls){return balls;}

    public float action(float paddleLength, float objectLength){
        return paddleLength;
    }

    public void resetTimer()
    {
        timerForPowerUP.resetTimer();
    }

    public TimerForPowerUP getTimerForPowerUP() {
        return timerForPowerUP;
    }

    public boolean isActive()
    {
        return timerForPowerUP.isTimerRunning();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractPowerUP that = (AbstractPowerUP) o;
        return action.equals(that.action);
    }

    @Override
    public int hashCode() {
        return Objects.hash(action);
    }
}
