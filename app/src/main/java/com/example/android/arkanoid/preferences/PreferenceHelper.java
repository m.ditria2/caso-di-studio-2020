package com.example.android.arkanoid.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

/**
 * This class helps the saving and reding of the Preferences
 */
public class PreferenceHelper {
    private static final String REFERENCE_KEY = "com.example.android.arkanoid";

    private static final String LANGUAGE_KEY = "language";
    private static final String EMPTY_STRING = "empty";
    private static final String USER_KEY = "user";
    private static final String CONTROLLER_KEY = "controller";
    private static final String SOUND_KEY = "sound";
    private static final String DIFFICULTY_KEY = "difficulty";

    /**
     * Accesses the Preferences and gets the value of {@link #LANGUAGE_KEY}
     *
     * @param context
     * @return a String Object containing the language
     */
    public static String getPreferenceLanguage(Context context) {
        String lang = "";
        SharedPreferences sharedPref = context.getSharedPreferences(REFERENCE_KEY, MODE_PRIVATE);
        if(Locale.getDefault().getDisplayLanguage().equals("italiano")){
            lang = "it";
        }else{
            lang = "en";
        }
        String prefLanguage = sharedPref.getString(LANGUAGE_KEY, lang  );
        return prefLanguage;
    }

    /**
     * Accesses the Preferences and sets the value of {@link #LANGUAGE_KEY}
     *
     * @param language: the language String to set in the value of {@link #LANGUAGE_KEY}
     * @param context
     */
    public static void setPreferenceLanguage(String language, Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(REFERENCE_KEY,MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        sharedPref.edit().remove(LANGUAGE_KEY).apply();
        editor.putString(LANGUAGE_KEY, language);
        editor.commit();
    }

    /**
     * Accesses the Preferences and sets the value of {@link #CONTROLLER_KEY}
     *
     * @param controller: the controller String to set in the value of {@link #CONTROLLER_KEY}
     * @param context
     */
    public static void setPreferenceController(String controller, Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(REFERENCE_KEY, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        sharedPref.edit().remove(CONTROLLER_KEY).apply();
        editor.putString(CONTROLLER_KEY, controller);
        editor.commit();
    }

    /**
     * Accesses the Preferences and gets the value of {@link #CONTROLLER_KEY}
     *
     * @param context
     * @return a String Object containing the value of {@link #CONTROLLER_KEY}
     */
    public static String getPreferenceController(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(REFERENCE_KEY, MODE_PRIVATE);
        String prefController = sharedPref.getString(CONTROLLER_KEY, EMPTY_STRING);
        return prefController;
    }

    /**
     * Accesses the Preferences and sets the value of {@link #USER_KEY}
     *
     * @param context
     * @param user: the user String to set in the value of {@link #USER_KEY}
     */
    public static void setUserLogged(Context context, String user){
        SharedPreferences prefs = context.getSharedPreferences(REFERENCE_KEY, MODE_PRIVATE);
        SharedPreferences.Editor user1 = prefs.edit().putString(USER_KEY, user);
        user1.commit();
    }

    /**
     * Accesses the Preferences and gets the value of {@link #USER_KEY}
     *
     * @param context
     * @return a String Object containing the value of {@link #USER_KEY}
     */
    public static String getUserLogged(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(REFERENCE_KEY, MODE_PRIVATE);
        return prefs.getString(USER_KEY,EMPTY_STRING);
    }

    /**
     * Checks if there is a logged user
     *
     * @param context
     * @return true if there is a user logged, else false
     */
    public static boolean isLogged(Context context){
        SharedPreferences prefs = context.getSharedPreferences(REFERENCE_KEY, MODE_PRIVATE);
        Log.d("", "isLogged: "+prefs.getString(USER_KEY,EMPTY_STRING));
        return !prefs.getString(USER_KEY,EMPTY_STRING).equals(EMPTY_STRING);
    }

    /**
     * Deletes the sharedPreference of the User
     * Used when a user logs out
     *
     * @param context
     */
    public static void deleteUserPref(Context context){
        SharedPreferences prefs = context.getSharedPreferences(REFERENCE_KEY,MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit().putString(USER_KEY, EMPTY_STRING);
        editor.commit();
    }

    /**
     * Accesses the Preferences and sets the value of {@link #SOUND_KEY}
     * (it can be "on"/"off")
     *
     * @param sound: the sound String to set in the value of {@link #SOUND_KEY}
     * @param context
     */
    public static void setSoundPref(String sound, Context context) {
        SharedPreferences prefs = context.getSharedPreferences(REFERENCE_KEY, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit().putString(SOUND_KEY, sound);
        editor.commit();
    }

    /**
     * Checks if the Shared Preference {@link #SOUND_KEY} is set to "off"
     *
     * @param context
     * @return
     */
    public static boolean isSoundPrefOff(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(REFERENCE_KEY, MODE_PRIVATE);
        return prefs.getString(SOUND_KEY, EMPTY_STRING).equals("off");
    }

    /**
     * Accesses the Preferences and sets the value of {@link #DIFFICULTY_KEY}     *
     *
     * @param context
     * @param difficulty: The string containing the value. It can be "easy"/"medium"/"hard"
     */
    public static void setPreferenceDifficulty(Context context, String difficulty) {
        SharedPreferences prefs = context.getSharedPreferences(REFERENCE_KEY, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit().putString(DIFFICULTY_KEY, difficulty);
        editor.commit();
    }

    /**
     * Accesses the Preferences and gets the value of {@link #DIFFICULTY_KEY}
     *
     * @param context
     * @return the String acquired
     */
    public static String getPreferenceDifficulty(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(REFERENCE_KEY, MODE_PRIVATE);
        return prefs.getString(DIFFICULTY_KEY, EMPTY_STRING);
    }
}
