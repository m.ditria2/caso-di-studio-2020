package com.example.android.arkanoid.game;

import android.content.Context;
import android.graphics.BitmapFactory;

import com.example.android.arkanoid.R;

public class Paddle extends DrawbleObject{

    public Paddle(Context context, float x, float y, float length, float height) {
        super(context, x, y, length, height);
        setBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.paddle_2));
    }

}
