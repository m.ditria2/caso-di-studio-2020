package com.example.android.arkanoid.game.editor;

import android.content.Context;
import android.util.Log;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * This class helps the managing of *.txt file saving of the level created
 */
public class FileHelper {

    /**
     * Writes the matrix of the level to the file "my_level.txt"
     *
     * @param context
     * @param data
     */
    public static void writeToFile(Context context, String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("my_level.txt", context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    /**
     * Reads the matrix from the file "my_level.txt"
     *
     * @param context
     * @return
     */
    public static int[][] readFromFile(Context context) {

        int[][] bricks = new int[8][7];
        try {
            InputStream inputStream = context.openFileInput("my_level.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i<bricks.length; i++) {
                    for (int j = 0; j < bricks[i].length;j++) {
                        if((receiveString = bufferedReader.readLine()) != null) {
                            String[] row = receiveString.split(" ");
                            bricks[i][j] = Integer.parseInt(row[2]);

                        }
                    }
                }

                inputStream.close();
                return bricks;

            } else return null;
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return null;
    }
}
