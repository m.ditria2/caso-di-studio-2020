package com.example.android.arkanoid.recyclerView;


import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.arkanoid.BasicGameActivity;
import com.example.android.arkanoid.R;

import java.util.ArrayList;

/**
 * Creates a {@link RecyclerView} which handles all the {@link Level} Objects available of the game
 *
 * @see com.example.android.arkanoid.databaseHelper.DatabaseHelper
 * @see Level
 */
public class LevelRecViewAdapter extends RecyclerView.Adapter<LevelRecViewAdapter.ViewHolder> {
    private ArrayList<Level> levelAL = new ArrayList<>();

    public LevelRecViewAdapter(){
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.level_list_item, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.btnLevel.setText(levelAL.get(i).getLevel());
        if(levelAL.get(i).isCompleted()) {
            viewHolder.btnLevel.setEnabled(true);
            viewHolder.btnLevel.setBackgroundResource(R.drawable.level);
        } else {
            viewHolder.btnLevel.setEnabled(false);
            viewHolder.btnLevel.setBackgroundResource(R.drawable.lock_level);

        }
        viewHolder.btnLevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), BasicGameActivity.class);
                intent.putExtra("lifes",3);
                intent.putExtra("score",0);
                intent.putExtra("level",Integer.parseInt(levelAL.get(i).getLevel()) -1);
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return levelAL.size();
    }

    public void setLevel(ArrayList<Level> level) {
        this.levelAL = level;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private final Button btnLevel;

        public ViewHolder(@NonNull View itemView){
            super(itemView);
            btnLevel = itemView.findViewById(R.id.btnLevel);

        }
    }

}
