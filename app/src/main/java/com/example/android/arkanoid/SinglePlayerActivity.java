package com.example.android.arkanoid;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.arkanoid.databaseHelper.DatabaseHelper;
import com.example.android.arkanoid.formWindowHelper.FormWindow;
import com.example.android.arkanoid.preferences.PreferenceHelper;
import com.example.android.arkanoid.preferences.SoundHelper;
import com.example.android.arkanoid.recyclerView.Level;
import com.example.android.arkanoid.recyclerView.LevelRecViewAdapter;

import java.util.ArrayList;


public class SinglePlayerActivity extends AppCompatActivity {

    private RecyclerView levelRecView;

    private DatabaseHelper db;
    private SQLiteDatabase database;
    private Button btnScore, btnCreateLevel, btnUnlock;
    private RadioButton rbEasy, rbMedium, rbHard;
    private final ArrayList<Level> levelAL = new ArrayList<>();
    private RadioGroup rgLevel;
    private LevelRecViewAdapter adapter;


    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_player);

        db = new DatabaseHelper(this);
        levelRecView = findViewById(R.id.levelRecView);
        rgLevel = findViewById(R.id.rgLevel);
        rbEasy = findViewById(R.id.rbEasy);
        rbMedium = findViewById(R.id.rbMedium);
        rbHard = findViewById(R.id.rbHard);
        btnScore = findViewById(R.id.btnScore);
        btnCreateLevel = findViewById(R.id.btnCreateLevel);
        btnUnlock = findViewById(R.id.btnUnlock);



        setRadioButtonDifficulty();

        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        if (PreferenceHelper.isLogged(this)) {
            myToolbar.setTitle(PreferenceHelper.getUserLogged(this));
        }
        adapter = new LevelRecViewAdapter();

        setLevelArray();

        adapter.setLevel(levelAL);

        levelRecView.setAdapter(adapter);
        levelRecView.setLayoutManager(new GridLayoutManager(this, 2));

        rgLevel.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                resetAllButtons();
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = group.findViewById(checkedId);

                setDifficulty(checkedRadioButton.getId());
                // If the radiobutton that has changed in check state is now checked...
                checkedRadioButton.setBackgroundResource(R.drawable.button_click);
            }
        });

        btnScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PreferenceHelper.isLogged(getApplicationContext())) {
                    Intent intent = new Intent(SinglePlayerActivity.this, ScoreActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(SinglePlayerActivity.this, getResources().getString(R.string.score_message), Toast.LENGTH_LONG).show();
                }
            }
        });

        btnCreateLevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PreferenceHelper.isLogged(getApplicationContext())) {
                    Intent intent = new Intent(SinglePlayerActivity.this, EditLevelActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(SinglePlayerActivity.this, getResources().getString(R.string.create_level_message), Toast.LENGTH_LONG).show();
                }
            }
        });

        btnUnlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.unlockAllLevels();
                recreate();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this,MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        this.menu = menu;
        MenuItem itemSignup = menu.findItem(R.id.itemSignup);
        MenuItem itemLogin = menu.findItem(R.id.itemLogin);
        MenuItem itemLogout = menu.findItem(R.id.itemLogout);
        if (PreferenceHelper.isLogged(getApplicationContext())) {
            itemSignup.setVisible(false);
            itemLogin.setVisible(false);
            itemLogout.setVisible(true);
        } else {
            itemSignup.setVisible(true);
            itemLogin.setVisible(true);
            itemLogout.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itemLogin:{
                if (FormWindow.window == null) {
                    FormWindow.createWindow("login", this);
                    FormWindow.window.show();
                }
                break;
            }

            case R.id.itemSignup: {
                if (FormWindow.window == null) {
                    FormWindow.createWindow("signup", this);
                    FormWindow.window.show();
                }
                break;
            }

            case R.id.itemLogout: {
                PreferenceHelper.deleteUserPref(getApplicationContext());
                FormWindow.reload(this);
                break;
            }
            case R.id.itemSettings: {
                Intent intent = new Intent(SinglePlayerActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        }
        return super.onOptionsItemSelected(item);
    }


    public void setDifficulty(int id) {
        switch (id) {
            case R.id.rbEasy: {
                PreferenceHelper.setPreferenceDifficulty(this,"easy");
                break;
            }
            case R.id.rbMedium: {
                PreferenceHelper.setPreferenceDifficulty(this,"medium");
                break;
            }
            case R.id.rbHard: {
                PreferenceHelper.setPreferenceDifficulty(this,"hard");
                break;
            }
            default: {
                break;
            }
        }
    }

    private void resetAllButtons() {
        rbEasy.setBackgroundResource(R.drawable.button);
        rbMedium.setBackgroundResource(R.drawable.button);
        rbHard.setBackgroundResource(R.drawable.button);
    }

    private void setRadioButtonDifficulty() {
        switch (PreferenceHelper.getPreferenceDifficulty(this)) {
            case "easy": {
                rbEasy.setBackgroundResource(R.drawable.button_click);
                break;
            }
            case "medium": {
                rbMedium.setBackgroundResource(R.drawable.button_click);
                break;
            }
            case "hard": {
                rbHard.setBackgroundResource(R.drawable.button_click);
                break;
            }
            default: {
                PreferenceHelper.setPreferenceDifficulty(this, "medium");
                rbMedium.setBackgroundResource(R.drawable.button_click);
                break;
            }
        }
    }

    private void setLevelArray() {
        database = db.getWritableDatabase();
        String query = "SELECT * FROM levels";
        Cursor data = database.rawQuery(query, null);
        while (data.moveToNext()) {
            levelAL.add(new Level(data.getString(0), data.getInt(1)));
        }
        database.close();
    }

    @Override
    protected void onPause() {
        super.onPause();
        SoundHelper.pause(R.raw.suono_home);
        if(FormWindow.window != null){
            if (FormWindow.window.isShowing()) {
                FormWindow.window.dismiss();
                FormWindow.window = null;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!PreferenceHelper.isSoundPrefOff(this)) {
            SoundHelper.resume(R.raw.suono_home);
        }

        levelAL.clear();
        setLevelArray();

        adapter.setLevel(levelAL);
        adapter.notifyDataSetChanged();
    }

}