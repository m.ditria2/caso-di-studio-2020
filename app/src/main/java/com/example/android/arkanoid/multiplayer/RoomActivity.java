package com.example.android.arkanoid.multiplayer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.arkanoid.MainActivity;
import com.example.android.arkanoid.R;
import com.example.android.arkanoid.preferences.PreferenceHelper;
import com.example.android.arkanoid.preferences.SoundHelper;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * This activity handle the creation of the room
 * or the entry to an existing one
 * (in Multiplayer mode)
 *
 * @see com.example.android.arkanoid.MultiGameActivity
 */
public class RoomActivity extends AppCompatActivity {
    ListView listView;

    Button btnCreateRoom;

    List<String> roomsList;

    String playerName = "";
    String roomName = "";
    private int count;


    private TextView title;
    FirebaseDatabase database;
    DatabaseReference roomRef;
    DatabaseReference roomsRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);

        title = findViewById(R.id.Text);


        database = FirebaseDatabase.getInstance();
        // get the player name and assign his room name to the player name
        playerName = PreferenceHelper.getUserLogged(this);
        roomName = playerName;
        listView = findViewById(R.id.listView);
        btnCreateRoom = findViewById(R.id.btnCreateRoom);

        // all existing available room
        roomsList = new ArrayList<>();

        btnCreateRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO mettere lo strings
                //btnCreateRoom.setText("CREATING ROOM");
                if(isNetworkConnected())
                {
                    btnCreateRoom.setEnabled(false);
                    roomName = playerName;
                    roomRef = database.getReference("rooms/" + roomName + "/player1");
                    addRoomEventListener();
                    roomRef.setValue(playerName);
                }
                else Toast.makeText(RoomActivity.this, getResources().getString(R.string.no_connection_room), Toast.LENGTH_SHORT).show();

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // join an existing room and add yourself as player2
                roomName = roomsList.get(position);
                roomRef = database.getReference("rooms/" + roomName + "/player2");
                addRoomEventListener();
                roomRef.setValue(playerName);
            }

        });
        // show if new room is available
        addRoomsEventListener();
    }

    /**
     * set room listener, it allows the user to join a room
     */
    private void addRoomEventListener(){
        roomsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                // join the room
                //btnCreateRoom.setText("CREATE ROOM");
                btnCreateRoom.setEnabled(true);
                Intent intent = new Intent(getApplicationContext(),TempConnectActivity.class);
                intent.putExtra("roomName", roomName);
                startActivity(intent);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                //error
                //btnCreateRoom.setText("CREATE ROOM");
                btnCreateRoom.setEnabled(true);
                Toast.makeText(RoomActivity.this, "Error!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Synchronize the Rooms resting on the Firebase Realtime Database
     * with the rooms displayed in the activity
     */
    private void addRoomsEventListener(){
        roomsRef = database.getReference("rooms");
        roomsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot datasnapshot) {
                // show list of rooms
                roomsList.clear();
                count=0;
                Iterable<DataSnapshot> rooms = datasnapshot.getChildren();
                for(DataSnapshot snapshot : rooms){
                    if(!snapshot.getKey().equals(" ") && !snapshot.getKey().equals(playerName)){
                    roomsList.add(snapshot.getKey());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(RoomActivity.this, android.R.layout.simple_list_item_1,roomsList);
                    listView.setAdapter(adapter);
                    count++;
                }
                setTitleValue();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.d("TAG", "onCancelled: ERROR!!!!");
            }
        });
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
    @Override
    protected void onPause() {
        super.onPause();
        SoundHelper.pause(R.raw.suono_home);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!PreferenceHelper.isSoundPrefOff(this)) {
            SoundHelper.resume(R.raw.suono_home);
        }
    }

    /**
     * Plurals implemented for the number of the rooms
     */
    private void setTitleValue() {
        count -= 1;
        if(count==0){
            title.setText(getResources().getString(R.string.select_room_zero));
        } else {
            Resources res = getResources();
            title.setText(res.getQuantityString(R.plurals.create_room, count));
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}